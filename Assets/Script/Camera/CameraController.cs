﻿
using UnityEngine.UI;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private Transform player;
    [SerializeField]
    private Transform entropy;
    [SerializeField]
    private float acercamiento = 15;
    [SerializeField]
    private Image inicioPanel;
    [SerializeField]
    private Text textoCountdown;

    private Vector3 orPosition;
    private int maxTime = 3;
    private float timer = 0;

	// Use this for initialization
	void Start () {
        orPosition = transform.position;
        timer = maxTime;
        textoCountdown.text = "";

        EstadosDelJuego.Instancia.EnInicio(true);  //Importante, poner un check de esto para hacer que los vehiculos puedan moverse o actuar (en false para que se muevan, en true para que no)
    }

    private void LateUpdate()
    {
        if (inicioPanel.color.a > 0)
        {
            return;
        }

        Vector3 entro = new Vector3(entropy.position.x, entropy.position.y, orPosition.z);
        Vector3 play = new Vector3(player.position.x, player.position.y, orPosition.z);


        if (timer > 0)
        {
            if (timer <= 1)
            {
                if ((timer + 0.5f).ToString("0") != "0")
                {
                    textoCountdown.text = (timer + 0.5f).ToString("0");
                }
                GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, 20, Time.fixedUnscaledDeltaTime / 0.4f);
                transform.position = Vector3.Lerp(transform.position, orPosition, Time.fixedUnscaledDeltaTime / 0.3f);
            }
            else if (timer <= 2)
            {
                textoCountdown.text = (timer + 0.5f).ToString("0");
                GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, acercamiento, Time.fixedUnscaledDeltaTime / 0.4f);
                transform.position = Vector3.Lerp(transform.position, play, Time.fixedUnscaledDeltaTime / 0.2f);
            }
            else if (timer <= 3)
            {
                if ((timer + 0.5f).ToString("0") != "4")
                {
                    textoCountdown.text = (timer + 0.5f).ToString("0");
                }
                GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, acercamiento, Time.fixedUnscaledDeltaTime / 0.4f);
                transform.position = Vector3.Lerp(transform.position, entro, Time.fixedUnscaledDeltaTime / 0.2f);
            }
            timer -= Time.deltaTime;
        }
        else
        {
            textoCountdown.text = "";
            GetComponent<Camera>().orthographicSize = 20;
            timer = 0;
            transform.position = orPosition;
            EstadosDelJuego.Instancia.EnInicio(false); //Importante, poner un check de esto para hacer que los vehiculos puedan moverse o actuar (en false para que se muevan, en true para que no)
            GetComponent<CameraController>().enabled = false;
        }
    }
}
