﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {

    public static int VecesQueEmpiezo = 0;
    public static bool primeraCarga;

	void Awake ()
    {
        primeraCarga = false;
        VecesQueEmpiezo++;
        Invoke("Espera", 0.5f);

	}

    private void Espera()
    {
        primeraCarga = true;
        DataSerializer.Instancia.LoadJson();
        DataSerializer.Instancia.SendLoaded();
    }

}
