﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.IO;
using System;

public class DataSerializer {


    private static DataSerializer instacia = null;

    public static DataSerializer Instancia
    {
        get
        {
            if (instacia == null)
            {
                instacia = new DataSerializer();
            }
            return instacia;
        }
    }

    private string fileName = "data.json";
    private InventoryManager inventory;

    public DataSerializer()
    {
        if (inventory == null)
        {
            inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<InventoryManager>();
        }

    }

    public class Data
    {
        public float[] lastDateMission = new float[5];
        public int[] buttonIds = new int[5];
        public int[] buttonsComplete = new int[5];

        public int energy;
        public double lastDate;
        public Inventory inventory;

        public Data()
        {
            if (inventory == null)
            {
                inventory = new Inventory();
            }
        }
    }

    [Serializable]
    public class Inventory
    {
        public int currency;
        public int hardCurrency;
        public Vehicle[] vehicle = new Vehicle[3];
        public Core[] coreInventory = new Core[50];
        public Wheels[] wheelsInventory = new Wheels[50];
        public Weapon[] weaponInventory = new Weapon[50];

        public Inventory()
        {
            if (vehicle[0] == null)
            {
                for (int i = 0; i < vehicle.Length; i++)
                {
                    vehicle[i] = new Vehicle();
                }

                for (int i = 0; i < coreInventory.Length; i++)
                {
                    if (coreInventory[i] == null)
                    {
                        coreInventory[i] = new Core();
                    }
                }

                for (int i = 0; i < wheelsInventory.Length; i++)
                {
                    if (wheelsInventory[i] == null)
                    {
                        wheelsInventory[i] = new Wheels();
                    }
                }

                for (int i = 0; i < weaponInventory.Length; i++)
                {
                    if (weaponInventory[i] == null)
                    {
                        weaponInventory[i] = new Weapon();
                    }
                }
            }
        }
    }

    [Serializable]
    public class Vehicle
    {
        public int numTipo;
        public bool funcionando;
        public Core core;
        public Wheels wheels;
        public Weapon weapon;

        public Vehicle()
        {
            if (core == null)
            {
                core = new Core();
            }
            if (wheels == null)
            {
                wheels = new Wheels();
            }
            if (weapon == null)
            {
                weapon = new Weapon();
            }
        }
    }

    [Serializable]
    public class Core
    {
        public string name;
        public int id;
        public float desgaste;
    }

    [Serializable]
    public class Wheels
    {
        public string name;
        public int id;
        public float desgaste;
    }

    [Serializable]
    public class Weapon
    {
        public string name;
        public int id;
        public float desgaste;
    }

    private Data data;
    private bool devolverData;

    public void Write(int numdeVehiculo, Tipo type, bool funcionando = true)
    {
        if (data == null)
        {
            data = new Data();
            try
            {
                //LoadJson();
                //return;
            }
            catch (SystemException)
            {
                Debug.Log("File deleted");
            }
        }

        data.inventory.vehicle[numdeVehiculo].numTipo = (int)type;
        data.inventory.vehicle[numdeVehiculo].funcionando = funcionando;
        WriteToJson(data);
    }

    public void Write(DateTime _lastTimeClosed)
    {
        if (data == null)
        {
            data = new Data();
            try
            {
                //LoadJson();
                //return;
            }
            catch (SystemException)
            {
                Debug.Log("File deleted");
            }
        }
        data.lastDate = _lastTimeClosed.ToOADate();
        WriteToJson(data);
    }

    public void Write(int numDeCurrency = 0, bool isHard = false)
    {
        if (data == null)
        {
            data = new Data();
        }

            if (!isHard)
        {
            if (numDeCurrency > 0)
            {
                data.inventory.currency = numDeCurrency;
            }
        }
        else
        {
            if (numDeCurrency > 0)
            {
                data.inventory.hardCurrency = numDeCurrency;
            }
        }

        WriteToJson(data);
    }

    public void Write(BaseObject objeto, int numdeVehiculo)
    {
        if (data == null)
        {
            data = new Data();
            try
            {
                //LoadJson();
                //return;
            }
            catch (SystemException)
            {
                Debug.Log("File deleted");
            }
        }
        if (objeto == null)
        {
            data.inventory.vehicle[numdeVehiculo] = null;
            WriteToJson(data);
            return;
        }

        if (objeto.GetType() == typeof(CoreObject))
        {
            data.inventory.vehicle[numdeVehiculo].core.name = objeto.name;
            data.inventory.vehicle[numdeVehiculo].core.desgaste = objeto.desgastePropio;
        }
        else if (objeto.GetType() == typeof(WheelsObject))
        {
            data.inventory.vehicle[numdeVehiculo].wheels.name = objeto.name;
            data.inventory.vehicle[numdeVehiculo].wheels.desgaste = objeto.desgastePropio;
        }
        else if (objeto.GetType() == typeof(WeaponObject))
        {
            data.inventory.vehicle[numdeVehiculo].weapon.name = objeto.name;
            data.inventory.vehicle[numdeVehiculo].weapon.desgaste = objeto.desgastePropio;
        }
        else
        {
            Debug.Log("couldn't write");
        }

        WriteToJson(data);
    }

    public void WriteMissions(float timeClosed, int timeToComplete, int buttonID)
    {
        if (data == null)
        {
            data = new Data();
        }
        data.buttonsComplete[buttonID] = timeToComplete;
        data.lastDateMission[buttonID] = timeClosed;
        data.buttonIds[buttonID] = buttonID;
        WriteToJson(data);
    }


    public void WriteInventory(BaseObject parte, int position)
    {
        if (data == null)
        {
            data = new Data();
            try
            {
                //LoadJson();
                //return;
            }
            catch (SystemException)
            {
                Debug.Log("File deleted");
            }
        }

        if (parte.GetType() == typeof(CoreObject))
        {
            data.inventory.coreInventory[position].name = parte.name;
            data.inventory.coreInventory[position].desgaste = parte.desgastePropio;
        }
        else if (parte.GetType() == typeof(WheelsObject))
        {
            data.inventory.wheelsInventory[position].name = parte.name;
            data.inventory.wheelsInventory[position].desgaste = parte.desgastePropio;
        }
        else if (parte.GetType() == typeof(WeaponObject))
        {
            data.inventory.weaponInventory[position].name = parte.name;
            data.inventory.weaponInventory[position].desgaste = parte.desgastePropio;
        }
        else
        {
            Debug.Log("couldn't write");
        }

        WriteToJson(data);
    }


    public void WriteEnergyAtClosed(int _energy)
    {
        if (data == null)
        {
            data = new Data();
            try
            {
                //LoadJson();
                //return;
            }
            catch (SystemException)
            {
                Debug.Log("File deleted");
            }
        }
        data.energy = _energy;

        WriteToJson(data);
    }





    private void WriteToXML(Data objetoAEscribir)
    {
        string persistentPath = Path.Combine(Application.persistentDataPath, fileName);

        try
        {
            XmlSerializer dataSerializer = new XmlSerializer(typeof(Data));
            FileStream file = File.Open(persistentPath, FileMode.OpenOrCreate);
            dataSerializer.Serialize(file, objetoAEscribir);
            file.Close();
            Debug.Log(string.Format("Successfully saved data at [{0}]", persistentPath));
        }
        catch (System.Exception)
        {

            Debug.Log("Couldn't save file");
        }
    }

    private void WriteToJson(Data objetoAEscribir)
    {
        if (!Loader.primeraCarga)
        {
            return;
        }

        string persistentPath = Path.Combine(Application.persistentDataPath, fileName);
        string json = JsonUtility.ToJson(objetoAEscribir, true);

        File.WriteAllText(persistentPath, json);
    }

    public void LoadJson()
    {
        if (!Loader.primeraCarga)
        {
            return;
        }
        string filePath = Path.Combine(Application.persistentDataPath, fileName);

        if (data == null)
        {
            data = new Data();
        }
        Debug.Log(filePath);

        if (File.Exists(filePath))
        {
            try
            {
                string dataAsJson = File.ReadAllText(filePath);

                Data item = JsonUtility.FromJson<Data>(dataAsJson);
                data = item;
                devolverData = true;
            }
            catch (System.Exception ex)
            {
                Debug.LogError(string.Format("Cannot load game data, {0}", ex));
                devolverData = false;
            }

        }
        else
        {
            FirstTimeController.FirstTimeActions();
            Debug.Log("File does not exist");
            devolverData = false;
        }

    }

    public void SendLoaded()
    {
        if (!devolverData)
        {
            return;
        }
        for (int i = data.inventory.vehicle.Length - 1; i >= 0 ; i--)
        {
            if (data.inventory.vehicle[i].funcionando)
            {
                ControladorMotoTemporal.vehiculosEnInventario += 1;

                ControladorMotoTemporal.CambiarGuardado(i);
                if (data.inventory.vehicle[i].core.id == 0)
                {
                    ControladorMotoTemporal.Core(DiccionaryPartes.GetParte(data.inventory.vehicle[i].core.name, data.inventory.vehicle[i].core.desgaste) as CoreObject);
                }
                if (data.inventory.vehicle[i].wheels.id == 0)
                {
                    ControladorMotoTemporal.Wheels(DiccionaryPartes.GetParte(data.inventory.vehicle[i].wheels.name, data.inventory.vehicle[i].wheels.desgaste) as WheelsObject);
                }
                if (data.inventory.vehicle[i].weapon.id == 0)
                {
                    ControladorMotoTemporal.Weapons(DiccionaryPartes.GetParte(data.inventory.vehicle[i].weapon.name, data.inventory.vehicle[i].weapon.desgaste) as WeaponObject);
                }

                ControladorMotoTemporal.ChangeSpriteByType(data.inventory.vehicle[i].numTipo);

                inventory.SetVehicles(i);
            }
            else
            {
                inventory.SetVehicles(i, true);
            }
        }

        CurrencyController.Instancia.LoadCurrency(data.inventory.currency, data.inventory.hardCurrency);

        DeviceTimeChecker.Instancia.LastTimeClosed = DateTime.FromOADate(data.lastDate);

        EnergyDisponibility.Instancia.LoadEnergy= data.energy;

        MissionButton[] missionButtons = UnityEngine.Object.FindObjectsOfType<MissionButton>();
        for (int i = 0; i < missionButtons.Length; i++)
        {
            for (int f = 0; f < missionButtons.Length; f++)
            {
                missionButtons[f].ReloadMissions(data.lastDateMission[i], data.buttonsComplete[i], data.buttonIds[i]);
            }
        }

    }


    public void DeleteData()
    {
        string filePath = Path.Combine(Application.persistentDataPath, fileName);

        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
    }
}
