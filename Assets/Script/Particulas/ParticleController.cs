﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour {

    [SerializeField]
    private ParticleSystem particles;

    private List<ParticleController> instatiatedObjects = new List<ParticleController>();

    public void EmmitParticles(Vector2 newPosition, Quaternion newRotation)
    {
        instatiatedObjects.Add(Instantiate(this.gameObject, newPosition, newRotation).GetComponent<ParticleController>());
        instatiatedObjects[instatiatedObjects.Count - 1].particles.Play();
        if (!IsInvoking())
        {
            InvokeRepeating("UpdateParticles", 1, 1);
        }
    }

    private void UpdateParticles()
    {
        for (int  i = 0; i < instatiatedObjects.Count; i++)
        {
            if (instatiatedObjects[i].particles.isStopped)
            {
                instatiatedObjects.RemoveAt(i);
            }
        }

        if (instatiatedObjects.Count <= 0)
        {
            CancelInvoke();
        }
    }
}
