﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFunctionObserver : MonoBehaviour {

    [SerializeField]
    private ParticleController particleControllerPlayer;
    [SerializeField]
    private ParticleController particleControllerEntropy;

    // Use this for initialization
    void Awake () {
        TriggerTrail.EnChoqueJugador += PonerNuevoParticulasJugador;
        TriggerBordes.EnChoqueJugadorBorde += PonerNuevoParticulasJugador;

        TriggerTrail.EnChoqueEnemigo += PonerNuevoParticulasEnemigo;
        TriggerBordes.EnChoqueEnemigoBorde += PonerNuevoParticulasEnemigo;
    }

    private void OnDestroy()
    {
        TriggerTrail.EnChoqueJugador -= PonerNuevoParticulasJugador;
        TriggerBordes.EnChoqueJugadorBorde -= PonerNuevoParticulasJugador;

        TriggerTrail.EnChoqueEnemigo -= PonerNuevoParticulasEnemigo;
        TriggerBordes.EnChoqueEnemigoBorde -= PonerNuevoParticulasEnemigo;
    }

    public void PonerNuevoParticulasJugador(Vector2 newPosition, Quaternion newRotation)
    {
        particleControllerPlayer.EmmitParticles(newPosition, newRotation);
    }

    public void PonerNuevoParticulasEnemigo(Vector2 newPosition, Quaternion newRotation)
    {
        particleControllerEntropy.EmmitParticles(newPosition, newRotation);
    }
}
