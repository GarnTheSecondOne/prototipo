﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Parts/Weapon")]
[Serializable]
public class WeaponObject : BaseObject
{
    public int numProyectiles = 3;
    public float frecuenciaDisparo = 5;
    public float magnitudDisparo = 3;
    public float alcanceDisparo = 0.2f;
    public DisparadorScript.NaturalezaArma naturaleza = DisparadorScript.NaturalezaArma.hole;
}
