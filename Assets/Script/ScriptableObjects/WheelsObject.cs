﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Wheels", menuName = "Parts/Wheel")]
[Serializable]
public class WheelsObject : BaseObject
{
    public float aceleration = 8;
    public int maxTrail = 50;
}
