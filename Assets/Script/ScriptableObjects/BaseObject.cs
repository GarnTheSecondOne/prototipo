﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BaseObject : ScriptableObject {
    public string objectName = "New Object";
    public string icon = "icom_base_1"; //Diccionario, Resources, Dictionary<string, Sprite>
    public float desgastePropio = 0;

    [HideInInspector]
    public int id = 0;

    private void Awake()
    {
        if (id != 0)
        {
            DiccionaryPartes.AddNewPart(id, this);
        }
    }
}
