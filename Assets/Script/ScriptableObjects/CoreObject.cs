﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Core", menuName = "Parts/Core")]
[Serializable]
public class CoreObject : BaseObject
{
    public float desgaste = 1;
    public float maxVelocity = 2;
    public float disminucionAlGirar = 5;
    public float disminucionAlDisparar = 50;
    public float knockback = 2;
}
