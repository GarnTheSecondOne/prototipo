﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Vehicle Slot", menuName = "Vehicles/Slot")]
[Serializable]
public class VehicleObject : ScriptableObject
{
    public bool funcionando = true;
    public Tipo type = Tipo.moto;
    public CoreObject coreBase;
    public WeaponObject weaponBase;
    public WheelsObject wheelsBase;

    [NonSerialized]
    public CoreObject core;
    [HideInInspector]
    public WeaponObject weapon;
    [HideInInspector]
    public WheelsObject wheels;

    private void Awake()
    {
        if (core == null)
        {
            core = coreBase;
        }
        if (weapon == null)
        {
            weapon = weaponBase;
        }
        if (wheels == null )
        {
            wheels = wheelsBase;
        }
    }

    private void OnEnable()
    {
        if (core == null)
        {
            core = coreBase;
        }
        if (weapon == null)
        {
            weapon = weaponBase;
        }
        if (wheels == null)
        {
            wheels = wheelsBase;
        }
    }

    public void SetDefaults()
    {
        core = coreBase;

        weapon = weaponBase;

        wheels = wheelsBase;
    }
}
