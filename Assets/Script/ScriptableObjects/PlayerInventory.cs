﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory")]
[Serializable]
public class PlayerInventory : ScriptableObject {
    public int currencyNormal = 0;
    public int currencyHard = 0;
    public List<VehicleObject> vehiculo = new List<VehicleObject>(3);
    public List<BaseObject> partes = new List<BaseObject>();
}
