﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiccionaryPartes : MonoBehaviour {

    [SerializeField]
    private List<CoreObject> cores = new List<CoreObject>();
    [SerializeField]
    private List<WeaponObject> weapons = new List<WeaponObject>();
    [SerializeField]
    private List<WheelsObject> wheels = new List<WheelsObject>();

    private static Dictionary<string, BaseObject> partes = new Dictionary<string, BaseObject>();

    private static List<int> ids = new List<int>();
    private static Dictionary<int, BaseObject> partesCreadas = new Dictionary<int, BaseObject>();

	// Use this for initialization
	void Start () {
		if (Loader.VecesQueEmpiezo < 2)
        {
            foreach (CoreObject core in cores)
            {
                partes.Add(core.name, core);
            }
            foreach (WeaponObject weapon in weapons)
            {
                partes.Add(weapon.name, weapon);
            }
            foreach (WheelsObject wheel in wheels)
            {
                partes.Add(wheel.name, wheel);
            }
        }
	}
	
    public static BaseObject GetParte(string key, float desgaste = 0)
    {
        try
        {
            partes[key].desgastePropio = desgaste;
            return partes[key];
        }
        catch
        {
            if (key != "")
            {
                Debug.LogError(key);
            }
            return null;
        }
    }

    public static void AddNewPart(int key, BaseObject part)
    {
        partesCreadas.Add(key, part);
        ids.Add(key);
    }

    public static bool SearchKey(int _key)
    {
        if (ids.Contains(_key))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
