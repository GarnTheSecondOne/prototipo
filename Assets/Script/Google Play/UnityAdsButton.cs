﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{
#if UNITY_IOS
private string gameId = "1587117";
#elif UNITY_ANDROID
    private string gameId = "1587116";
#endif

    Button m_Button;

    private ReferencesToScriptsForAds references;

    public string placementId = "rewardedVideo";

    void Start()
    {
        references = GetComponent<ReferencesToScriptsForAds>();

        m_Button = GetComponent<Button>();
        if (m_Button) m_Button.onClick.AddListener(ShowAd);

        if (Advertisement.isSupported)
        {
            Advertisement.Initialize(gameId, true);
        }
    }

    void Update() {
        if (!references.pressed && references.working)
        {
            try
            {
                if (m_Button) m_Button.interactable = Advertisement.IsReady(placementId);
            }
            catch
            {
                //Why
            }
        }
        else
        {
            m_Button.interactable = false;
        }
    }

    void ShowAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;
        Advertisement.Show(placementId, options);
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            references.OnlyOnce();
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}