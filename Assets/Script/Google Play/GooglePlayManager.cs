﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GooglePlayManager : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        GooglePlayGames.PlayGamesPlatform.Activate();

        if (!Social.localUser.authenticated)
        {
            Log("Authenticating...");
            Social.localUser.Authenticate(success =>
            {
                if (success)
                {
                    Log("Welcome " + Social.localUser.userName);
                }
                else
                {
                    Log("Authentication failed.");
                }
            });
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void OnLoginCliked()
    {
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate(success =>
            {
                if (success)
                {
                    Log("Welcome " + Social.localUser.userName);
                }
                else
                {
                    Log("Authentication failed.");
                }
            });
        }
        else
        {
            Log("Signing out.");
            ((GooglePlayGames.PlayGamesPlatform)Social.Active).SignOut();
        }
    }

    public void UnlockAchievement(string achievement)
    {
        if (achievement == "Craft")
        {
            Social.ReportProgress(GPGSIds.achievement_craft_a_vehicle, 100, success =>
            {
                success = true;
                if (success)
                {
                    Log("Achievement Unlocked");
                }
                else
                {
                    Log("Achievement Unlocked failed :(");
                }
            });
        }
        else if (achievement == "Win")
        {
            Social.ReportProgress(GPGSIds.achievement_your_first_victory, 100, success =>
            {
                success = true;
                if (success)
                {
                    Log("Achievement Unlocked");
                }
                else
                {
                    Log("Achievement Unlocked failed :(");
                }
            });
        }
    }


    private void Log(string value)
    {
        Debug.Log(value);
        //logText.text += ("\n" + value);
    }

    public void ShowAchievements()
    {
        Social.ShowAchievementsUI();
    }

    public void ShowLeaderboard()
    {
        Social.ShowLeaderboardUI();
    }
}
