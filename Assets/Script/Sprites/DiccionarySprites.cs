﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiccionarySprites : MonoBehaviour {

    [SerializeField]
    private Sprite[] spritesCoreMoto;
    [SerializeField]
    private Sprite[] spritesCoreAlpha;
    [SerializeField]
    private Sprite[] spritesCoreBeta;

    [SerializeField]
    private Sprite[] spritesWheelsMoto;
    [SerializeField]
    private Sprite[] spritesWheelsAlpha;
    [SerializeField]
    private Sprite[] spritesWheelsBeta;

    [SerializeField]
    private Sprite[] spritesWeaponsMoto;
    [SerializeField]
    private Sprite[] spritesWeaponsAlpha;
    [SerializeField]
    private Sprite[] spritesWeaponsBeta;

    public static DiccionarySprites instance = null;
    private static bool alreadyAdded = false;

    private static Dictionary<string, Sprite> dic = new Dictionary<string, Sprite>();

    private void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        if (Loader.VecesQueEmpiezo < 2 && !alreadyAdded)
        {
            AddSprites(Tipo.moto);
            AddSprites(Tipo.carroBeta);
            AddSprites(Tipo.carroAlfa);
        }
    }

    public void AddSprites(Tipo carType)
    {
        alreadyAdded = true;
        switch (carType)
        {
            case Tipo.moto:
                for (int i = 0; i < spritesCoreMoto.Length; i++)
                {
                    dic.Add(string.Format("icon_core_{0}" + carType.ToString(), i + 1), spritesCoreMoto[i]);
                }
                for (int i = 0; i < spritesWheelsMoto.Length; i++)
                {
                    dic.Add(string.Format("icon_wheels_{0}" + carType.ToString(), i + 1), spritesWheelsMoto[i]);
                }
                for (int i = 0; i < spritesWeaponsMoto.Length; i++)
                {
                    dic.Add(string.Format("icon_weapon_{0}" + carType.ToString(), i + 1), spritesWeaponsMoto[i]);
                }
                break;

            case Tipo.carroAlfa:
                for (int i = 0; i < spritesCoreAlpha.Length; i++)
                {
                    dic.Add(string.Format("icon_core_{0}" + carType.ToString(), i + 1), spritesCoreAlpha[i]);
                }
                for (int i = 0; i < spritesWheelsAlpha.Length; i++)
                {
                    dic.Add(string.Format("icon_wheels_{0}" + carType.ToString(), i + 1), spritesWheelsAlpha[i]);
                }
                for (int i = 0; i < spritesWeaponsAlpha.Length; i++)
                {
                    dic.Add(string.Format("icon_weapon_{0}" + carType.ToString(), i + 1), spritesWeaponsAlpha[i]);
                }
                break;

            case Tipo.carroBeta:
                for (int i = 0; i < spritesCoreBeta.Length; i++)
                {
                    dic.Add(string.Format("icon_core_{0}" + carType.ToString(), i + 1), spritesCoreBeta[i]);
                }
                for (int i = 0; i < spritesWheelsBeta.Length; i++)
                {
                    dic.Add(string.Format("icon_wheels_{0}" + carType.ToString(), i + 1), spritesWheelsBeta[i]);
                }
                for (int i = 0; i < spritesWeaponsBeta.Length; i++)
                {
                    dic.Add(string.Format("icon_weapon_{0}" + carType.ToString(), i + 1), spritesWeaponsBeta[i]);
                }
                break;

            default:        
                break;
        }
    }

    public static Sprite GetSprite(string sprite)
    {
        return dic[sprite];
    }

}
