﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReinicioController : MonoBehaviour {

    float time;
    float index;

    public delegate void Reiniciar();
    public static event Reiniciar enReinicio; 
    //Enviado a IAMovement, 

	void Start ()
    {
        time = 0; //tiempo para esperar
        index = 0;

        ChoqueDealer.enReiniciar += Reiniciando;
    }

    private void OnDestroy()
    {
        ChoqueDealer.enReiniciar -= Reiniciando;
    }

    void Update ()
    {
        if (ScoreController.continuarPartida)
        {
            if (time > 0f)
            {
                time += Time.deltaTime;  //La mejor y unica forma de esperar para hacer algo
                if (time > 3)
                {
                    enReinicio(); //Se envía el evento para reiniciar la partida
                    time = 0;
                    index = 0;
                }
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                enReinicio(); //Reinicio a la fuerza, se me olvidó que existía lol
            }
        }
	}

    //Llega del ChoqueDealer cuando uno de los vehiculos se choca
    void Reiniciando(int eso)
    {
        if (ScoreController.continuarPartida)
        {
            index += eso; //Para evitar que se "reinicie" más de una vez
            if (index > 1)
            {
                return;
            }

            time = 0.1f;
        }
    }
}
