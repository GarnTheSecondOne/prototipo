﻿
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

    [SerializeField]
    private Text playerText;
    [SerializeField]
    private Text entropyText;

    private static int vidas = 3;
    private static int vidasEntro = 5;

    //private static int scoreJugador;
    //private static int scoreEntropy;

    private static int lifesLeft;
    private static int entroLifesLeft;

    public static bool continuarPartida;


    public delegate void TerminarPartida(bool ganar);
    public static event TerminarPartida EnFinalDelJuego;

    public delegate void EventosComunes();
    public static event EventosComunes EnMenorVida;

    private void Start()
    {
        //scoreEntropy = 0;
        //scoreJugador = 0;
        InventoryManager inventoryManager = InventoryManager.Instance();
        int newVidas = 0;
        for (int i = 0; i < inventoryManager.inventory.vehiculo.Count; i++)
        {
            if (inventoryManager.inventory.vehiculo[i].funcionando)
            {
                newVidas++;
            }
        }


        vidas = newVidas;
        lifesLeft = vidas;
        entroLifesLeft = vidasEntro;

        EstadosDelJuego.Instancia.terminado = false;
        ShowScores();

        continuarPartida = true;

        ChoqueDealer.enGanar += PlayerWin;
        ChoqueDealer.enPerder += PlayerLose;
    }

    private void Restart()
    {
        //scoreEntropy = 0;
        //scoreJugador = 0;
        vidas = VehiculoActual.numDeVehiculos;
        lifesLeft = vidas;
        EstadosDelJuego.Instancia.terminado = false;
        ShowScores();

        continuarPartida = true;
    }

    public static void SetRondas(int vidasNuevas)
    {
        vidas = vidasNuevas;
    }

    private void PlayerWin()
    {
        ScoreUp(true);
    }

    private void PlayerLose()
    {
        ScoreUp(false);
    }

    private void ScoreUp(bool ganador)
    {
        if (ganador)
        {
            //scoreJugador++;
            entroLifesLeft--;
        }
        else
        {
            //scoreEntropy++;
            lifesLeft--;
        }

        ShowScores();
        CheckWinability();
        if (!ganador && continuarPartida)
        {
            EnMenorVida();
        }
    }

    private void ShowScores()
    {
        playerText.text = lifesLeft.ToString();
        entropyText.text = entroLifesLeft.ToString();
    }

    private void CheckWinability()
    {
        if (lifesLeft <= 0)
        {
            EstadosDelJuego.Instancia.terminado = true;
            continuarPartida = false;
            EnFinalDelJuego(false);
        }
        else if (entroLifesLeft <= 0)
        {
            EstadosDelJuego.Instancia.terminado = true;
            continuarPartida = false;
            EnFinalDelJuego(true);
        }

        //if (scoreJugador > (vidas / 2))
        //{
        //    continuarPartida = false;
        //    EnFinalDelJuego(true);

        //}
        //else if (scoreEntropy > (vidas / 2))
        //{
        //    continuarPartida = false;
        //    EnFinalDelJuego(false);
        //}
    }

    private void OnDestroy()
    {
        ChoqueDealer.enGanar -= PlayerWin;
        ChoqueDealer.enPerder -= PlayerLose;
    }
}
