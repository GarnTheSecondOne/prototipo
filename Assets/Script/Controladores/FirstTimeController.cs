﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTimeController : MonoBehaviour {

    public static bool firstTime = false;

    public static void FirstTimeActions()
    {
        firstTime = true;

        InventoryManager inventoryManager = InventoryManager.Instance();

        for (int i = 0; i < InventoryManager.Instance().inventory.vehiculo.Count; i++)
        {
            inventoryManager.inventory.vehiculo[i].SetDefaults();
            inventoryManager.inventory.vehiculo[i].type = (Tipo)i;
            inventoryManager.inventory.vehiculo[i].funcionando = true;
        }
    }
}
