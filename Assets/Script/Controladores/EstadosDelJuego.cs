﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadosDelJuego {


    private static EstadosDelJuego instacia = null;
    private bool iniciando;
    public bool terminado;
    public bool invulnerable;

    public static EstadosDelJuego Instancia
    {
        get
        {
            if (instacia == null)
            {
                instacia = new EstadosDelJuego();
            }
            return instacia;
        }
    }

    public bool Iniciando
    {
        get
        {
            return iniciando;
        }
    }

    public void EnInicio(bool _inicio)
    {
        iniciando = _inicio;
    }


}
