﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminarController : MonoBehaviour {

    [SerializeField]
    private PauseUIController pausa;
    [SerializeField]
    private InventoryManager inventory;
    [SerializeField]
    private TestModalWindow testModal;

	// Use this for initialization
	void Start () {
        ScoreController.EnFinalDelJuego += Terminar;
	}

    private void OnDestroy()
    {
        ScoreController.EnFinalDelJuego -= Terminar;
    }

    private void Terminar(bool haGanado)
    {
        if (haGanado)
        {
            AccionesAlGanar();
        } 
        else
        {
            AccionesAlPerder();
        }
        Invoke("Pausa", 3);

        for (int i = 0; i < inventory.inventory.vehiculo.Count; i++)
        {
            if (inventory.inventory.vehiculo[i].funcionando)
            {
                DataSerializer.Instancia.Write(inventory.inventory.vehiculo[i].core, i);
                DataSerializer.Instancia.Write(inventory.inventory.vehiculo[i].weapon, i);
                DataSerializer.Instancia.Write(inventory.inventory.vehiculo[i].wheels, i);
                DataSerializer.Instancia.Write(i, inventory.inventory.vehiculo[i].type, true);
            }
        }
        DataSerializer.Instancia.Write(CurrencyController.Currency, false);
        DataSerializer.Instancia.Write(CurrencyController.CurrencyHard, true);
    }

    private void Pausa()
    {
        pausa.Pausar();
    }

    private void AccionesAlGanar()
    {
        GooglePlayManager googlePlayManager = FindObjectOfType<GooglePlayManager>();
        googlePlayManager.UnlockAchievement("Win");

        float rand = UnityEngine.Random.Range(0, 1);
        if (rand < 0.5f)
        {
            testModal.TestTakeLeave();
        }

        CurrencyController.Instancia.GainCurrency(100, false);
    }

    private void AccionesAlPerder()
    { 
        CurrencyController.Instancia.GainCurrency(10, false);
    }

}
