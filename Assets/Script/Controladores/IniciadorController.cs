﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniciadorController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        EstadosDelJuego.Instancia.EnInicio(true);
	}
}
