﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBordes : MonoBehaviour {

    /// <summary>
    /// Código usado para que los muros maten. Poco más
    /// </summary>

    public delegate void LlegoAlBorde(int conCuantos);
    public static event LlegoAlBorde EnBotChoque;
    public static event LlegoAlBorde EnTuChoque;

    public delegate void OcurreColisionEspecial(Vector2 position, Quaternion velocity);
    public static event OcurreColisionEspecial EnChoqueJugadorBorde;
    public static event OcurreColisionEspecial EnChoqueEnemigoBorde;

    private Vector3 vectorJugador = Vector3.zero;
    //Se envía a ChoqueDealer(?)

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            {
                if (!EstadosDelJuego.Instancia.invulnerable)
                {

                    if (vectorJugador == Vector3.zero)
                    {
                        vectorJugador = collision.gameObject.transform.position;
                    }
                    collision.gameObject.GetComponent<Rigidbody2D>().AddForce(-collision.gameObject.GetComponent<Rigidbody2D>().velocity);
                    //collision.gameObject.transform.position = vectorJugador;
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (!EstadosDelJuego.Instancia.invulnerable)
            {
                EnChoqueJugadorBorde(other.transform.position, (Quaternion.LookRotation(other.GetComponent<Rigidbody2D>().velocity, Vector3.forward)));
                EnTuChoque(1); //Se chocó el jugador(1); //si es el jugador el que entró
            }
            else
            {
                if (vectorJugador == Vector3.zero)
                {
                    vectorJugador = other.gameObject.transform.position;
                }
                other.gameObject.GetComponent<Rigidbody2D>().AddForce(-other.gameObject.GetComponent<Rigidbody2D>().velocity * 100);
                //other.gameObject.transform.position = vectorJugador;
            }
        }
        if (other.CompareTag("Enemy"))
        {
            EnChoqueEnemigoBorde(other.transform.position, (Quaternion.LookRotation(other.GetComponent<Rigidbody2D>().velocity, Vector3.forward)));
            EnBotChoque(1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            vectorJugador = Vector3.zero;
        }
    }
}
