﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EntropyActor : MonoBehaviour {
	NavMeshAgent pathfinder;
	Transform player;
	Vector3 d;
	float t;
	float maxT = 10;

	enum EntropyState { Chilled, Triggered, Shooting, Waiting };
	EntropyState state;

	public float speed;

	float rate = 2f;
	float changeTime = 10f;
	float currentTime;
	int timesChanged;
	int timesToChange = 4;

	bool sw = false;

	void Start () {
		currentTime = 0;
		timesChanged = 0;
		player = GameObject.FindWithTag("Player").transform;
		state = EntropyState.Chilled;
		pathfinder = GetComponent<NavMeshAgent>();
		pathfinder.speed = speed;
		//pathfinder.SetDestination(Vector3.zero + new Vector3(-28, 14,0
	}

	void Update () {
		switch (state) {
			case EntropyState.Chilled:
				beingChill();
				break;

			case EntropyState.Shooting:
				break;

			case EntropyState.Triggered:
				beingTriggered();
				break;

			case EntropyState.Waiting:
				break;
		}
	}

	void beingChill() {

		currentTime += Time.deltaTime * 0.1f;

		if (sw == false || currentTime > changeTime || transform.position == pathfinder.destination) {
			pathfinder.SetDestination(getRandomPosition());
			Debug.Log(pathfinder.destination);
			sw = true;
		}

		if (currentTime > changeTime) {
			timesChanged++;
			currentTime = 0;
		}

		if (timesChanged > timesToChange) {
			timesChanged = 0;
			currentTime = 0;
			state = EntropyState.Triggered;
		}
	}

	float getRandomChangeTime() {
		return changeTime + Random.Range(-rate, rate);
	}

	Vector3 getRandomPosition() {
		return new Vector3(Random.Range(-30, 30), Random.Range(-15, 15), 0);
	}

	void beingTriggered() {
		if (true) {
			pathfinder.speed = speed;
			pathfinder.SetDestination(player.transform.position);
		}
	}

	void wait() {
		pathfinder.speed = 0;
	}
}
