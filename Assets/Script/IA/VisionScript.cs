﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionScript : MonoBehaviour {

	/// <summary>
	/// Includo en la IA, se usa un trigger para que sea su "visión"
	/// El codigo detecta que algo entra en el campo de visión y le envía un evento a IAMovement
	/// </summary>

	public delegate void ViendoAlgo();
	public static event ViendoAlgo onDanger;
	//Evento que sucede cuando se detecta algo en el trigger 

	private void OnTriggerStay2D(Collider2D other) {
		onDanger(); //Enviado a IAMovement
	}
}
