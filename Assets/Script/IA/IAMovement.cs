﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAMovement : MonoBehaviour {
    /// Código usado para el comportamiento de la IA, movimiento y rotación de su sprite

    [SerializeField]
    private float maxVelocity = 1000; //Velocidad maxima a la que llegará
    [SerializeField]
    public float aceleracion; //aceleración con la que empezará a moverse
    [SerializeField]
    public float cambios = 10; //cada cuanto hay un cambio


    private float h, v;
    private Rigidbody2D rb;
    private bool funcionar;

    private Vector2 origen;
    private Quaternion originalRotation;

    private Vector2 movement;
    private float angulo;

    private bool cambio;
    private float tiempoDeEspera;
    private bool peligrando;

    void Start ()
    {
        try
        {
            rb = GetComponent<Rigidbody2D>(); //Detecta el Rigidbody2D del objeto
        }
        catch
        {
            print("No rigidbody2d on IA");
        }
        funcionar = true; //está funcionando en el momento
        StartRigidbody();  //Inicia el rigidbody, para que tenga los valores deseados
        cambio = false; //no debe cambiar
        tiempoDeEspera = 0; //tiempo a esperar para el proximo cambio
        movement = new Vector2(0,0.5f); //inicializando el primer vector de movimiento
        angulo = Mathf.Atan2(movement.y, movement.x) * Mathf.Rad2Deg; //el angulo actual en el que se encuentra el objeto
        origen = transform.position;
        originalRotation = transform.rotation;
        peligrando = false; //no está detectando peligros

        //Eventos a los cuales está suscrito este script
        VisionScript.onDanger += ViendoPeligro;
        TriggerTrail.EnChoque += Parar;
        TriggerTrail.EnChocado += Parar;
        ReinicioController.enReinicio += Reinicio;
        ChoqueDealer.enReiniciar += Parar;
    }

    private void OnDestroy()
    {
        VisionScript.onDanger -= ViendoPeligro;
        TriggerTrail.EnChoque -= Parar;
        TriggerTrail.EnChocado -= Parar;
        ReinicioController.enReinicio -= Reinicio;
        ChoqueDealer.enReiniciar -= Parar;
    }

    //Para organizar aspectos importantes del rigidbody
    private void StartRigidbody() 
    {
        if (rb)
        {
            rb.gravityScale = 0; //gravedad 0, para que no se caiga 
        }
    }

    void Update ()
    {
        if (!funcionar || EstadosDelJuego.Instancia.Iniciando)
        {
            return;
        }
        Movement();

        if (!cambio)
        {
            tiempoDeEspera += Time.deltaTime;
            if (tiempoDeEspera >= cambios)
            {
                cambio = true;
                tiempoDeEspera = 0;
            }
        }
    }

    //Hacer que se mueva
    private void Movement()
    {
        if (cambio || peligrando) //Si está peligrando o está en cambio, Cambia de Dirección
        {
            movement = ChangeDirection();
            movement = movement.normalized;
            cambio = false;
            peligrando = false;
        }

        rb.velocity = Vector2.Lerp(rb.velocity, (movement * maxVelocity * 10), (Time.deltaTime * aceleracion)); //para que tenga que acelerar
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angulo, Vector3.forward), Time.deltaTime * aceleracion); //se cambia la rotación basado en la aceleración
    }

    //Obligar a que el objeto pare
    private void Parar(int cuantasVeces) 
    {
        int i = 0;
        if (i < 1 && funcionar == true) //forma mediocre de evitar que llegue varias veces el evento
        {
            i += cuantasVeces;
            rb.velocity = Vector2.zero;
            funcionar = false;
        }
    }

    //Álgo apareció en el campo de visión y es un peligro
    private void ViendoPeligro()
    {
        if (!peligrando && funcionar)
        {
            peligrando = true;
            Movement();
        }
    }

    //Método que da una dirección nueva al objeto cuando es necesario
    private Vector2 ChangeDirection()
    {
        Vector2 newDirection = new Vector2(0,0);

        newDirection = new Vector2(Random.Range(-0.8f, 0.8f), Random.Range(-0.8f, 0.8f)); //la nueva dirección es similar a darle valores de "h" y de "v", usando un random
        angulo = Mathf.Atan2(newDirection.y, newDirection.x) * Mathf.Rad2Deg; //angulo de la nueva dirección (no se para qué lo saqué, puede ser que no se necesite)
        float entreAngulo = Vector2.Angle(rb.velocity, newDirection); //el angulo entre la dirección actual y la nueva dirección
        if (entreAngulo > 100) 
        {
            while (entreAngulo > 100) //si el angulo anterior es mayor a 100, se vuelve a crear direcciones hasta que la condición sea falsa
            {
                newDirection = new Vector2(Random.Range(-0.8f, 0.8f), Random.Range(-0.8f, 0.8f));
                angulo = Mathf.Atan2(newDirection.y, newDirection.x) * Mathf.Rad2Deg;
                entreAngulo = Vector2.Angle(rb.velocity, newDirection);
            }
        }

        return newDirection; //Devuelve la nueva dirección (wow)
    }

    //Reinicia todos los elementos del objeto a su estado original
    private void Reinicio()
    {
        funcionar = true;
        transform.position = origen;
        transform.rotation = originalRotation;
        cambio = false;
        tiempoDeEspera = 0;
        movement = new Vector2(0, 0.5f);
        angulo = Mathf.Atan2(1, 0) * Mathf.Rad2Deg;
        peligrando = false;
    }

    //Evento que se llama cuando se es golpeado con una bala
    private void Knockback(Vector2 direccion)
    {
        rb.AddForce(direccion/MovementPlayer.knockbackStabilizer, ForceMode2D.Impulse); //se le añade una fuerza, se tiene en cuenta el estabilizador del jugador en este caso
    }
}
