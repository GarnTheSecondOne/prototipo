﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour {

    public static bool tutorialHasBeenDone;
    public static bool shootingHasBeenDone;

    [SerializeField]
    private Image myPanel;
    [SerializeField]
    private Image myJoystick;
    [SerializeField]
    private Image myAnimation;

    [SerializeField]
    private Text textMovement;
    [SerializeField]
    private Text textShooting;
    [SerializeField]
    private Text textTutorial;

    // Use this for initialization
    void Awake () {
        tutorialHasBeenDone = false;
        shootingHasBeenDone = false;

        myAnimation.gameObject.SetActive(false);
        myJoystick.gameObject.SetActive(false);
        textMovement.gameObject.SetActive(false);

        textTutorial.gameObject.SetActive(false);
        textShooting.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void LateUpdate () {

        if (!FirstTimeController.firstTime)
        {
            return;
        }

        if (!EstadosDelJuego.Instancia.Iniciando && !tutorialHasBeenDone && !shootingHasBeenDone)
        {
            myAnimation.gameObject.SetActive(true);
            myJoystick.gameObject.SetActive(true);
            textMovement.gameObject.SetActive(true);
        }

        if (tutorialHasBeenDone && shootingHasBeenDone)
        {
            myAnimation.gameObject.SetActive(false);
            myJoystick.gameObject.SetActive(false);
            textMovement.gameObject.SetActive(false);
            textShooting.gameObject.SetActive(false);

            textTutorial.gameObject.SetActive(true);
            HideText();
        }
        else if (shootingHasBeenDone)
        {
            textShooting.gameObject.SetActive(false);
        }
        else if (tutorialHasBeenDone)
        {
            myAnimation.gameObject.SetActive(false);
            myJoystick.gameObject.SetActive(false);
            textMovement.gameObject.SetActive(false);

            textShooting.gameObject.SetActive(true);
        }
	}

    private void HideText()
    {
        Color Alpha = new Color(textTutorial.color.r, textTutorial.color.g, textTutorial.color.b, 0);
        if (textTutorial.gameObject.activeInHierarchy)
        {
            textTutorial.color = Color.Lerp(textTutorial.color, Alpha, Time.deltaTime / 5);
        }

        if (textTutorial.color.a <= 0)
        {
            myAnimation.gameObject.SetActive(false);
            myJoystick.gameObject.SetActive(false);
            textTutorial.gameObject.SetActive(false);
            myPanel.gameObject.SetActive(false);
        }
    }
}
