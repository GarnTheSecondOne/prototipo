﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyController {


    private static PlayerInventory inventory;


    private static CurrencyController instacia = null;

    public static CurrencyController Instancia
    {
        get
        {
            if (instacia == null)
            {
                instacia = new CurrencyController();
            }
            return instacia;
        }
    }

    public static int Currency
    {
        get
        {
            return inventory.currencyNormal;
        }
    }

    public static int CurrencyHard
    {
        get
        {
            return inventory.currencyHard;
        }
    }

    public void SetInventory(PlayerInventory _inventory)
    {
        if (inventory == null)
        {
            inventory = _inventory;
        }
    }

    public void LoadCurrency(int oldCurrencyNormal, int oldCurrencyHard)
    {
        inventory.currencyNormal = oldCurrencyNormal;
        inventory.currencyHard = oldCurrencyHard;
    }

    public void SpendCurrency(int aGastar, bool isHard)
    {
        if (!isHard)
        {
            inventory.currencyNormal -= aGastar;
            if (inventory.currencyNormal <= 0)
            {
                inventory.currencyNormal = 0;
            }
        }
        else
        {
            inventory.currencyHard -= aGastar;
            if (inventory.currencyHard <= 0)
            {
                inventory.currencyHard = 0;
            }
        }
    }

    public void GainCurrency(int aGanar, bool isHard)
    {
        if (!isHard)
        {
            inventory.currencyNormal += aGanar;
        }
        else
        {
            inventory.currencyHard += aGanar;
        }
    }
}
