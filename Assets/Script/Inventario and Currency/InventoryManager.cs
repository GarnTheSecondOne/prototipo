﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.Serialization;
using System;

public class InventoryManager : MonoBehaviour {

    public PlayerInventory inventory;
    public static int vehiclesInInventory;

    private static InventoryManager modalPanel;

    public static InventoryManager Instance()
    {
        if (!modalPanel)
        {
            modalPanel = FindObjectOfType(typeof(InventoryManager)) as InventoryManager;
            if (!modalPanel)
                Debug.LogError("There needs to be at least one GameObject with an InventoryManager script attached to it!");
        }

        return modalPanel;
    }

    // Use this for initialization
    void Start () {
        SendToCurrencyController();
        for (int  i = 0; i < inventory.vehiculo.Count; i++)
        {
           if (inventory.vehiculo[i].funcionando)
            {
                vehiclesInInventory++;
            }
        }
	}
	
	// Update is called once per frame
	private void SendToCurrencyController () {
        CurrencyController.Instancia.SetInventory(inventory);
	}

    public void SetVehicles(int numSlot)
    {
        if (numSlot < inventory.vehiculo.Count && inventory.vehiculo[numSlot] != null)
        {
            inventory.vehiculo[numSlot].funcionando = true;
            inventory.vehiculo[numSlot].core = ControladorMotoTemporal.coreAUsar;
            inventory.vehiculo[numSlot].weapon = ControladorMotoTemporal.weaponAUsar;
            inventory.vehiculo[numSlot].wheels = ControladorMotoTemporal.wheelsAUsar;
            inventory.vehiculo[numSlot].type = (Tipo)ControladorMotoTemporal.numDeTipo;
            ControladorMotoTemporal.Vehiculo(inventory.vehiculo[numSlot], numSlot);
            vehiclesInInventory = SearchWhichWork();
        }
    }

    private int SearchWhichWork()
    {
        int checkWorking = 0;
        for (int i = 0; i < inventory.vehiculo.Count; i++)
        {
            if (inventory.vehiculo[i].funcionando)
            {
                checkWorking++;
            }
        }
        return checkWorking;
    }

    public void SetVehicles(int numSlot, bool nulo)
    {
        if (nulo)
        {
            if (numSlot < inventory.vehiculo.Count)
            {
                inventory.vehiculo[numSlot].funcionando = false;
            }
        }
    }

    public void SetInventoryAgain()
    {
        vehiclesInInventory = inventory.vehiculo.Count;
    }

    public void AddVehicle(int numSlot)
    {
        if (inventory.vehiculo[numSlot] != null)
        {
            inventory.vehiculo[numSlot].funcionando = true;
            inventory.vehiculo[numSlot].SetDefaults();
            ControladorMotoTemporal.Core(inventory.vehiculo[numSlot].core);
            ControladorMotoTemporal.Weapons(inventory.vehiculo[numSlot].weapon);
            ControladorMotoTemporal.Wheels(inventory.vehiculo[numSlot].wheels);
        }
    }

    public void RemoveVehicle(int numSlot)
    {
        inventory.vehiculo[numSlot].funcionando = false;
        DataSerializer.Instancia.Write(numSlot, inventory.vehiculo[numSlot].type, false);
    }

    public void AddNewPart(BaseObject objeto)
    {
        Debug.Log("You took it");
        if (inventory.partes.Count < 50)
        {
            inventory.partes.Add(objeto);
            int position = inventory.partes.Count - 1;
            //DataSerializer.Instancia.WriteInventory(inventory.partes[position], position);
        }
        else
        {
            Debug.LogError("Too many parts");
        }
    }

    public void RemovePart(int position)
    {
        Debug.Log("Part Used");

        inventory.partes.RemoveAt(position);
    }
}

