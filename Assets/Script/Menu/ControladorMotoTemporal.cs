﻿using UnityEngine;

public class ControladorMotoTemporal {

    private static ControladorMotoTemporal instacia = null;

    public static ControladorMotoTemporal Instancia
    {
        get
        {
            if (instacia == null)
            {
                instacia = new ControladorMotoTemporal();
            }
            return instacia;
        }
    }


    //tipo de vehiculo
    public static Tipo tipo;


    ///uso de objetos
    public static CoreObject coreAUsar = null;
    public static WeaponObject weaponAUsar = null;
    public static WheelsObject wheelsAUsar = null;
    public static int numDeTipo = 0;

    public static VehicleObject[] vehicleAUsar = new VehicleObject[3];
    public static int vehiculosEnInventario = 0;
    public static int vehiculoGuardado = 0;
    //public static int numDeVehiculos = 1;

    //Eventos
    public delegate void CambioDeSprite(VehicleObject parametro);
    public static event CambioDeSprite EnCambioDeSprite;

    public static void Vehiculo(VehicleObject vehiculos, int numDeGuardado)
    {
        if (vehiculos.funcionando)
        {
            vehiculoGuardado = numDeGuardado;
            vehicleAUsar[vehiculoGuardado] = vehiculos;
            vehiculosEnInventario = InventoryManager.vehiclesInInventory;

            ChangeSpriteByType((int)vehicleAUsar[vehiculoGuardado].type);
            Core(vehicleAUsar[vehiculoGuardado].core);
            Weapons(vehicleAUsar[vehiculoGuardado].weapon);
            Wheels(vehicleAUsar[vehiculoGuardado].wheels);
        }
        else
        {
            ChangeSpriteToNull();
        }
    }

    public static void Vehiculo(VehicleObject vehiculo)
    {
        vehiculo.type = tipo;
        vehiculo.core = coreAUsar;
        vehiculo.wheels = wheelsAUsar;
        vehiculo.weapon = weaponAUsar;
    }

    public static void CambiarGuardado(int aCual)
    {
        vehiculoGuardado = aCual;
        vehiculosEnInventario = InventoryManager.vehiclesInInventory;
    }

    public static void Core(CoreObject coreNuevo)
    {
        coreAUsar = coreNuevo;

        if (vehicleAUsar[vehiculoGuardado] != null)
        {
            if (coreAUsar != null)
            {
                vehicleAUsar[vehiculoGuardado].core = coreAUsar;
            }
            else
            {
                vehicleAUsar[vehiculoGuardado].core = vehicleAUsar[vehiculoGuardado].coreBase;
            }
        }

        ChangeSprite();
        DataSerializer.Instancia.Write(coreAUsar, vehiculoGuardado);
    }



    public static void Wheels(WheelsObject wheelNuevo)
    {
        wheelsAUsar = wheelNuevo;
        if (vehicleAUsar[vehiculoGuardado] != null)
        {
            if (wheelsAUsar != null)
            {
                vehicleAUsar[vehiculoGuardado].wheels = wheelsAUsar;
            }
            else
            {
                vehicleAUsar[vehiculoGuardado].wheels = vehicleAUsar[vehiculoGuardado].wheelsBase;
            }
        }

        ChangeSprite();
        DataSerializer.Instancia.Write(wheelsAUsar, vehiculoGuardado);
    }



    public static void Weapons(WeaponObject weaponNuevo)
    {
        weaponAUsar = weaponNuevo;
        if (vehicleAUsar[vehiculoGuardado] != null)
        {
            if (weaponAUsar != null)
            {
                vehicleAUsar[vehiculoGuardado].weapon = weaponAUsar;
            }
            else
            {
                vehicleAUsar[vehiculoGuardado].weapon = vehicleAUsar[vehiculoGuardado].weaponBase;
            }
        }

        ChangeSprite();
        DataSerializer.Instancia.Write(weaponAUsar, vehiculoGuardado);
    }

    public static void ChangeSpriteByType(int num)
    {

        numDeTipo = num;
        switch (num)
        {
            case 0:
                tipo = Tipo.moto;
                break;

            case 1:
                tipo = Tipo.carroAlfa;
                break;

            case 2:
                tipo = Tipo.carroBeta;
                break;

            default:
                tipo = Tipo.moto;
                break;
        }
        ChangeSprite();
        DataSerializer.Instancia.Write(vehiculoGuardado, (Tipo)numDeTipo, true);
    }

    public static void ChangeSprite()
    {
        if (vehicleAUsar[vehiculoGuardado] != null && vehicleAUsar[vehiculoGuardado].funcionando)
        {
            EnCambioDeSprite(vehicleAUsar[vehiculoGuardado]);
        }
        else
        {
            ChangeSpriteToNull();
        }
    }

    public static void ChangeSpriteToNull()
    {
        EnCambioDeSprite(null);
    }
}
