﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PartScrollList : MonoBehaviour {

    public SimpleObjectPool buttonObjectPool;
    public Transform contentPanel;

    public GameObject scrollPanel;

    private static PartScrollList partScrollList;


    public static PartScrollList Instance()
    {
        if (!partScrollList)
        {
            partScrollList = FindObjectOfType(typeof(PartScrollList)) as PartScrollList;
            if (!partScrollList)
                Debug.LogError("There needs to be at least one GameObject with a PartScrollList script attached to it!");
        }

        return partScrollList;
    }

    public void LookAtObjects(string tipo, int currentVehicle)
    {
        scrollPanel.SetActive(true);

        for (int i = 0; i <= contentPanel.childCount; i++)
        {
            if (contentPanel.childCount > 0)
            {
                GameObject toRemove = contentPanel.transform.GetChild(0).gameObject;
                buttonObjectPool.ReturnObject(toRemove);
            }
        }

        if (contentPanel.childCount > 0)
        {
            for (int i = 0; i <= contentPanel.childCount; i++)
            {
                GameObject toRemove = contentPanel.transform.GetChild(0).gameObject;
                buttonObjectPool.ReturnObject(toRemove);
            }
        }

        switch (tipo)
        {
            case "Wheels":

                GameObject newFirstButton = buttonObjectPool.GetObject();
                newFirstButton.transform.SetParent(contentPanel);
                newFirstButton.transform.localScale = Vector3.one;

                ScrollButton firstScrollButton = newFirstButton.GetComponent<ScrollButton>();
                firstScrollButton.Setup(InventoryManager.Instance().inventory, 
                    InventoryManager.Instance().inventory.vehiculo[currentVehicle].wheelsBase, 
                    currentVehicle);

                for (int i = 0; i < InventoryManager.Instance().inventory.partes.Count; i++)
                {

                    if (InventoryManager.Instance().inventory.partes[i].GetType() == typeof(WheelsObject))
                    {
                        GameObject newButton = buttonObjectPool.GetObject();
                        newButton.transform.SetParent(contentPanel);
                        newButton.transform.localScale = Vector3.one;

                        ScrollButton scrollButton = newButton.GetComponent<ScrollButton>();
                        scrollButton.Setup(InventoryManager.Instance().inventory, i, currentVehicle);
                    }
                }
                break;

            case "Weapon":
                GameObject newnewFirstButton = buttonObjectPool.GetObject();
                newnewFirstButton.transform.SetParent(contentPanel);
                newnewFirstButton.transform.localScale = Vector3.one;

                ScrollButton firstfirstScrollButton = newnewFirstButton.GetComponent<ScrollButton>();
                firstfirstScrollButton.Setup(InventoryManager.Instance().inventory,
                    InventoryManager.Instance().inventory.vehiculo[currentVehicle].weaponBase,
                    currentVehicle);


                for (int i = 0; i < InventoryManager.Instance().inventory.partes.Count; i++)
                {
                    if (InventoryManager.Instance().inventory.partes[i].GetType() == typeof(WeaponObject))
                    {
                        GameObject newButton = buttonObjectPool.GetObject();
                        newButton.transform.SetParent(contentPanel);
                        newButton.transform.localScale = Vector3.one;

                        ScrollButton scrollButton = newButton.GetComponent<ScrollButton>();
                        scrollButton.Setup(InventoryManager.Instance().inventory, i, currentVehicle);
                    }
                }
                break;

            case "Core":
                GameObject newnewnewFirstButton = buttonObjectPool.GetObject();
                newnewnewFirstButton.transform.SetParent(contentPanel);
                newnewnewFirstButton.transform.localScale = Vector3.one;

                ScrollButton firstfirstfirstScrollButton = newnewnewFirstButton.GetComponent<ScrollButton>();
                firstfirstfirstScrollButton.Setup(InventoryManager.Instance().inventory,
                    InventoryManager.Instance().inventory.vehiculo[currentVehicle].coreBase,
                    currentVehicle);


                for (int i = 0; i < InventoryManager.Instance().inventory.partes.Count; i++)
                {
                    if (InventoryManager.Instance().inventory.partes[i].GetType() == typeof(CoreObject))
                    {
                        GameObject newButton = buttonObjectPool.GetObject();
                        newButton.transform.SetParent(contentPanel);
                        newButton.transform.localScale = Vector3.one;

                        ScrollButton scrollButton = newButton.GetComponent<ScrollButton>();
                        scrollButton.Setup(InventoryManager.Instance().inventory, i, currentVehicle);
                    }
                }
                break;

            default:
                Debug.LogError("Something went Wrong");
                break;
        }
    }


    public void CloseScroll()
    {
        scrollPanel.SetActive(false);
    }
}
