﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollButton : MonoBehaviour {

    public Button button;
    public Text nameText;
    public Image iconImage;

    private BaseObject currentObject;
    private int currentEdition;
    private PlayerInventory currentInventory;

    private ModalPanel modalPanel;

    // Use this for initialization
    private void Start ()
    {
        button.onClick.AddListener(GoToOtherPanel);

        modalPanel = ModalPanel.Instance();
    }
	
    public void Setup(PlayerInventory inventory, int position, int edition)
    {
        currentObject = inventory.partes[position];
        currentEdition = edition;
        currentInventory = inventory;

        nameText.text = inventory.partes[position].objectName;
        iconImage.sprite = DiccionarySprites.GetSprite(inventory.partes[position].icon + inventory.vehiculo[edition].type);
    }

    public void Setup(PlayerInventory inventory, BaseObject objectPart, int edition)
    {
        currentObject = objectPart;
        currentEdition = edition;
        currentInventory = inventory;

        nameText.text = objectPart.objectName;
        iconImage.sprite = DiccionarySprites.GetSprite(objectPart.icon + inventory.vehiculo[edition].type);
    }

    public void GoToOtherPanel()
    {
        DeactivateAtStart.activated = true;
        modalPanel.PartSelected(currentObject, () => { SelectedPart(currentObject); });
    }


    private void SelectedPart(BaseObject part)
    {
        ControladorMotoTemporal.CambiarGuardado(currentEdition);

        if (part.GetType() == typeof(CoreObject))
        {
            ControladorMotoTemporal.Core(part as CoreObject);
        }

        if (part.GetType() == typeof(WeaponObject))
        {
            ControladorMotoTemporal.Weapons(part as WeaponObject);
        }

        if (part.GetType() == typeof(WheelsObject))
        {
            ControladorMotoTemporal.Wheels(part as WheelsObject);
        }

        ControladorMotoTemporal.Vehiculo(currentInventory.vehiculo[currentEdition]);
    }
}
