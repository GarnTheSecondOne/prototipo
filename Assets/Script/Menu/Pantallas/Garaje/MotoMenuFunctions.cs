﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MotoMenuFunctions : MonoBehaviour {

    public Canvas main;
    public PartScrollList scrollPanel;

    [SerializeField]
    private InventoryManager inventoryMnager = null;
    [SerializeField]
    private Text slotText;
    [SerializeField]
    private int margen = 5;


    private PlayerInventory guardarInventario = null;

    private int editingNow = 0;
    private Vector3 posicion;

    private ModalPanel modalPanel;

    // Use this for initialization
    void Start()
    {
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        posicion = new Vector3(0, -Screen.height - margen, 0);
        transform.position = (posicion);
        guardarInventario = inventoryMnager.inventory;

        modalPanel = ModalPanel.Instance();
        scrollPanel = PartScrollList.Instance();

        ChangeVehicleEditor(0);
    }


    public void SeleccionarCore(CoreObject core)
    {
        DeactivateAtStart.activated = true;
        scrollPanel.LookAtObjects("Core", editingNow);
    }


    public void SeleccionarWeapons(WeaponObject weapon)
    {
        DeactivateAtStart.activated = true;
        scrollPanel.LookAtObjects("Weapon", editingNow);
        //////modalPanel.PartSelected(weapon, () => { SelectedPart(weapon); });

        //ControladorMotoTemporal.CambiarGuardado(editingNow);
        //ControladorMotoTemporal.Weapons(weapon);
        //ControladorMotoTemporal.Vehiculo(guardarInventario.vehiculo[editingNow]);
    }


    public void SeleccionarWheels(WheelsObject wheel)
    {
        DeactivateAtStart.activated = true;
        scrollPanel.LookAtObjects("Wheels", editingNow);
        //modalPanel.PartSelected(wheel, () => { SelectedPart(wheel); });

        //ControladorMotoTemporal.CambiarGuardado(editingNow);
        //ControladorMotoTemporal.Wheels(wheel);
        //ControladorMotoTemporal.Vehiculo(guardarInventario.vehiculo[editingNow]);
    }


    public void SeleccionarType(int num)
    {
        ControladorMotoTemporal.CambiarGuardado(editingNow);
        ControladorMotoTemporal.ChangeSpriteByType(num);
        ControladorMotoTemporal.Vehiculo(guardarInventario.vehiculo[editingNow]);
    }

    public void ChangeVehicleEditor(int i)
    {
        editingNow += i;
        if (editingNow >= guardarInventario.vehiculo.Count)
        {
            editingNow = 0;
        }
        if (editingNow < 0)
        {
            editingNow = guardarInventario.vehiculo.Count - 1;
        }

        slotText.text = "Slot " + (editingNow + 1).ToString();
        SeleccionarVehicle(editingNow);
    }

    public void SeleccionarVehicle(int numDeVehiculo)
    {
        editingNow = numDeVehiculo;
        if (numDeVehiculo < guardarInventario.vehiculo.Count)
        {
            ControladorMotoTemporal.Vehiculo(guardarInventario.vehiculo[numDeVehiculo], numDeVehiculo);
        }
    }


    public void Hecho()
    {
        GooglePlayManager googlePlayManager = FindObjectOfType<GooglePlayManager>();
        googlePlayManager.UnlockAchievement("Craft");

        main.gameObject.SetActive(true);

        MovimientoSingle.Mover(main.transform.position);
    }


}
