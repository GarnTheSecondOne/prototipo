﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteVehicleChanger : MonoBehaviour {

    /// <summary>
    /// 0 sigma/moto
    /// 1 Alpha
    /// 2 Beta
    /// </summary>

    [SerializeField]
    public Sprite spriteNulo; //{ get; private set;}
    [SerializeField]
    private Image wheelsIcon;
    [SerializeField]
    private Image coreIcon;
    [SerializeField]
    private Image weaponIcon;

    private void Start()
    {
        ControladorMotoTemporal.EnCambioDeSprite += ChangeSprite;
    }

    private void OnDestroy()
    {
        ControladorMotoTemporal.EnCambioDeSprite -= ChangeSprite;
    }

    public void ChangeSprite(VehicleObject spriteElegido)
    {
        Color alpha;

        if (spriteElegido == null)
        {
            alpha = new Color(1, 1, 1, 0);

            wheelsIcon.color = alpha;
            weaponIcon.color = alpha;

            alpha = new Color(1, 1, 1, 0.5f);
            coreIcon.color = alpha;
            coreIcon.sprite = spriteNulo;
        }
        else
        {
            alpha = new Color(1, 1, 1, 1);

            wheelsIcon.color = alpha;
            coreIcon.color = alpha;
            weaponIcon.color = alpha;

            wheelsIcon.sprite = DiccionarySprites.GetSprite(spriteElegido.wheels.icon + spriteElegido.type);
            coreIcon.sprite = DiccionarySprites.GetSprite(spriteElegido.core.icon + spriteElegido.type);
            weaponIcon.sprite = DiccionarySprites.GetSprite(spriteElegido.weapon.icon + spriteElegido.type);
        }
    }

}
