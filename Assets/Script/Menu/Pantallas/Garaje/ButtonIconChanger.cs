﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonIconChanger : MonoBehaviour {

    [SerializeField]
    private BaseObject image;

    private Image myButton;

	// Use this for initialization
	void Start () {
        if (image != null)
        {
            myButton = GetComponent<Image>();
            try
            {
                myButton.sprite = DiccionarySprites.GetSprite(image.icon + Tipo.moto);
            }
            catch 
            {
                Debug.Log(string.Format("Key '{0}' doesn't exist", image.icon));
            }
        }
	}

}
