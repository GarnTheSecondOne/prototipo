﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelMenuFunctions : MonoBehaviour {

    [SerializeField]
    private EnergyManager energyManager;
    [SerializeField]
    private int energyToWaste = 20;
    [SerializeField]
    private int margen = 5;

    public Canvas main;

    //public Slider energySlider;

    public Text energyText;

    private Vector3 posicion;

    // Use this for initialization
    void Start()
    {
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        posicion = new Vector3(Screen.width + margen, 0, 0);
        transform.position = (posicion);
        EnergyManager.EnCambioDeEnergia += UpdateEnergySlider;
    }

    public void Jugar()
    {
        if (EnergyDisponibility.Instancia.currentEnergy >= energyToWaste)
        {
            energyManager.WasteEnergy(energyToWaste);
            SceneManager.LoadScene("TestScene");
        }
    }


    public void UpdateEnergySlider()
    {
        energyText.text = string.Format("Energy: {0}", EnergyDisponibility.Instancia.currentEnergy);
    }

    public void RegresarAMenu()
    {
        main.gameObject.SetActive(true);

        MovimientoSingle.Mover(main.transform.position);

        //this.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        UpdateEnergySlider();
    }

    private void OnDestroy()
    {
        EnergyManager.EnCambioDeEnergia -= UpdateEnergySlider;
    }

}
