﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpcionesFunctions : MonoBehaviour {

    public Canvas main;
    [SerializeField]
    private int margen = 5;

    private Vector3 posicion;

    void Start()
    {
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        posicion = new Vector3(-Screen.width - margen, 0, 0);
        transform.position = (posicion);
    }

    public void RegresarAMenu()
    {
        main.gameObject.SetActive(true);

        MovimientoSingle.Mover(main.transform.position);
    }
}
