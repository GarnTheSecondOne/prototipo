﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionButton : MonoBehaviour {

    [SerializeField]
    public int id;
    [SerializeField]
    public Text missionTimeText;
    [SerializeField]
    private Text missionWonText;

    private int timeForMission;
    private float timeLeft = 0f;

    private bool missionWon;

    private Button myButton;


	// Use this for initialization
	void Start ()
    {
        timeLeft = 0f;
        missionWon = false;
        myButton = GetComponent<Button>();

        missionTimeText.text = "";
        missionWonText.text = "";
    }

    private void LateUpdate()
    {
        if (timeLeft > 0 && !missionWon)
        {
            //Debug.Log("Tiempo que falta: " + timeLeft);
            //Debug.Log("De: " + timeForMission);

            myButton.interactable = false;

            timeLeft += Time.deltaTime;

            TimeSpan t = TimeSpan.FromSeconds(timeForMission - timeLeft);
            missionTimeText.text = string.Format("Time left: {0:D2}:{1:D2}",
                t.Minutes,
                t.Seconds);

            if (timeLeft >= timeForMission)
            {
                WinMission();
            }
        }
    }

    public void SetMission()
    {
        if (!missionWon)
        {
            timeForMission = UnityEngine.Random.Range(5 * 60, 15 * 60);
            myButton.interactable = false;
            timeLeft += Time.deltaTime;

            TimeSpan t = TimeSpan.FromSeconds(timeForMission);
            missionTimeText.text = string.Format("Time left: {0:D2}:{1:D2}",
                t.Minutes,
                t.Seconds);
            missionWonText.text = "On Mission...";
        }
        else
        {
            missionTimeText.text = "";
            int bits = UnityEngine.Random.Range(50, 150);
            missionWonText.text = string.Format("{0} bits received", bits);
            CurrencyController.Instancia.GainCurrency(bits, false);

            timeLeft = 0;
            timeForMission = 0;
            missionWon = false;
        }
    }

    public void WinMission()
    {
        missionWon = true;
        missionTimeText.text = string.Format("Time left: 00:00");
        missionWonText.text = "Mission Succesful!";

        timeLeft = 0;
        timeForMission = 0;
        myButton.interactable = true;
    }

    public void ReloadMissions(float lastTime, int timeToComplete, int whichId)
    {
        if (whichId == id)
        {
            timeForMission = timeToComplete;   
            timeLeft = lastTime;
            ReloadMissions();
        }
    }

    public void ReloadMissions()
    {
        if (timeForMission > 0)
        {
            float secondsSinceLast = (float)(DeviceTimeChecker.Instancia.TimeSpanSinceClosed().TotalSeconds);

            timeLeft += secondsSinceLast;

            missionWonText.text = "On Mission...";
        }
    }

    private void OnDestroy()
    {
        DataSerializer.Instancia.WriteMissions(timeLeft, timeForMission, id);
    }

    private void OnApplicationQuit()
    {
        DataSerializer.Instancia.WriteMissions(timeLeft, timeForMission, id);
    }
}
