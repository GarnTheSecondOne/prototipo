﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionsMenuFunctions : MonoBehaviour {

    public Canvas main;

    [SerializeField]
    private int margen = 5;

    private Vector3 posicion;
    private InventoryManager inventoryMnager = null;

    // Use this for initialization
    void Start ()
    {
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        posicion = new Vector3(0, Screen.height + margen, 0);
        transform.position = (posicion);
        inventoryMnager = InventoryManager.Instance();
    }

    public void Return()
    {
        main.gameObject.SetActive(true);

        MovimientoSingle.Mover(main.transform.position);
    }
}
