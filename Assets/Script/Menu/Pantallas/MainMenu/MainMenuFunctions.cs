﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuFunctions : MonoBehaviour {

    public Canvas niveles;
    public Canvas menuMotos;
    public Canvas opciones;
    public Canvas missions;

    // Use this for initialization
    void Start ()
    {
        GetComponent<RectTransform>().localPosition = Vector3.zero;
        transform.position = (Vector3.zero);
    }



    public void IniciarJuego()
    {
        niveles.gameObject.SetActive(true);

        MovimientoSingle.Mover(niveles.transform.position);

        ///this.gameObject.SetActive(false);
    }

    public void IrAMoto()
    {
        menuMotos.gameObject.SetActive(true);
        ControladorMotoTemporal.ChangeSprite();

        MovimientoSingle.Mover(menuMotos.transform.position);

        //this.gameObject.SetActive(false);
    }

    public void DeleteData()
    {
        DataSerializer.Instancia.DeleteData();

        UnityEngine.SceneManagement.SceneManager.LoadScene("MenuEscena");
    }

    public void IrAOpciones()
    {
        opciones.gameObject.SetActive(true);

        MovimientoSingle.Mover(opciones.transform.position);
    }

    public void GoToMissions()
    {
        missions.gameObject.SetActive(true);

        MovimientoSingle.Mover(missions.transform.position);
    }

    public void CerrarJuego()
    {
        Application.Quit();
    }
}
