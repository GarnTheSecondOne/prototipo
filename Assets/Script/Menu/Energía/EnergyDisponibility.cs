﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyDisponibility {

    private int maxEnergy = 100;

    public int currentEnergy = 100;

    private static EnergyDisponibility instacia = null;

    public static EnergyDisponibility Instancia
    {
        get
        {
            if (instacia == null)
            {
                instacia = new EnergyDisponibility();
            }
            return instacia;
        }
    }


    public int MaxEnergy
    {
        get
        {
            return maxEnergy;
        }
    }

    public int LoadEnergy
    {
        set
        {
            currentEnergy = value;
        }
    }
}
