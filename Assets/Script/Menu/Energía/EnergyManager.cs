﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyManager : MonoBehaviour {

    [SerializeField]
    private int secondsToRecharge = 60;

    private EnergyDisponibility disponibility;

    public delegate void EnergyChanged();
    public static event EnergyChanged EnCambioDeEnergia;

    private void Start()
    {
        disponibility = EnergyDisponibility.Instancia;

        if (Loader.VecesQueEmpiezo < 2)
        {
            Invoke("CheckHowMuchTimeHasPassed", 1);
        }
    }

    public int WasteEnergy(int _wastedEnergy = 20)
    {
        if (disponibility.currentEnergy < _wastedEnergy)
        {
            if (!IsInvoking("EnergyRecharge"))
            {
                InvokeRepeating("EnergyRecharge", 1, 10);
            }
            return disponibility.currentEnergy;
        }

        disponibility.currentEnergy -= _wastedEnergy;
        if (disponibility.currentEnergy <= 0)
        {
            disponibility.currentEnergy = 0;
        }

        InvokeRepeating("EnergyRecharge", 1, 10);

        if (DeviceTimeChecker.Instancia.EnoughtTimeHasPassed(secondsToRecharge))
        {
            Debug.Log("How the fuck"); //Esto no debería ocurrir
        }

        if (EnCambioDeEnergia != null)
        {
            EnCambioDeEnergia();
        }

        DataSerializer.Instancia.Write(DeviceTimeChecker.Instancia.Closing());
        DataSerializer.Instancia.WriteEnergyAtClosed(disponibility.currentEnergy);

        return disponibility.currentEnergy;
    }

    private void CheckHowMuchTimeHasPassed()
    {
        float rechargingPeriods = (float)(DeviceTimeChecker.Instancia.TimeSpanSinceClosed().TotalSeconds / secondsToRecharge);
        if (rechargingPeriods >= 1)
        {
            disponibility.currentEnergy += (1 * (int)rechargingPeriods);
            if (disponibility.currentEnergy >= 100)
            {
                disponibility.currentEnergy = 100;
                CancelInvoke();
            }
        }
        if (disponibility.currentEnergy < 100)
        {
            InvokeRepeating("EnergyRecharge", 1, 10);
        }
        if (EnCambioDeEnergia != null)
        {
            EnCambioDeEnergia();
        }
    }

    private void EnergyRecharge()
    {
        if (DeviceTimeChecker.Instancia.EnoughtTimeHasPassed(secondsToRecharge))
        {
            disponibility.currentEnergy += 1;
            if (disponibility.currentEnergy >= disponibility.MaxEnergy)
            {
                Debug.Log("Recargando");
                disponibility.currentEnergy = disponibility.MaxEnergy;
                CancelInvoke("EnergyRecharge");
            }
        }

        if (EnCambioDeEnergia != null)
        {
            EnCambioDeEnergia();
        }
    }

    public void EnergyRecharge(int _Recharge)
    {
        disponibility.currentEnergy += _Recharge;
        if (disponibility.currentEnergy >= 100)
        {
            disponibility.currentEnergy = 100;
            CancelInvoke();
        }

        if (EnCambioDeEnergia != null)
        {
            EnCambioDeEnergia();
        }
    }

    private void OnApplicationQuit()
    {
        DataSerializer.Instancia.Write(DeviceTimeChecker.Instancia.Closing());
        DataSerializer.Instancia.WriteEnergyAtClosed(disponibility.currentEnergy);
    }


}
