﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DeviceTimeChecker {

    private DateTime currentTime;
    private DateTime lastTimeChecked;
    private DateTime lastTimeClosed;

    private static DeviceTimeChecker instacia = null;

    public static DeviceTimeChecker Instancia
    {
        get
        {
            if (instacia == null)
            {
                instacia = new DeviceTimeChecker();
            }
            return instacia;
        }
    }

    private DeviceTimeChecker()
    {
        currentTime = DateTime.Now;
    }

    public DateTime CurrentTime
    {
        get
        {
            currentTime = DateTime.Now;
            return currentTime;
        }
    }

    public DateTime LastTimeClosed
    {
        get
        {
            return lastTimeClosed;
        }
        set
        {
            lastTimeClosed = value;
        }
    }

    public DateTime Closing()
    {
        lastTimeClosed = DateTime.Now;

        return lastTimeClosed;
    }

    public bool EnoughtTimeHasPassed(int secondsToCheck)
    {
        if (lastTimeChecked == DateTime.MinValue)
        {
            CheckTime();
        }

        TimeSpan howManySeconds = CurrentTime - lastTimeChecked;
        if (howManySeconds.TotalSeconds >= secondsToCheck)
        {
            lastTimeChecked = DateTime.MinValue;
            return true;
        }
        else
        {
            return false;
        }
    }

    public TimeSpan TimeSpanSinceClosed()
    {
        TimeSpan timeSpanned = CurrentTime - lastTimeClosed;
        return timeSpanned;
    }

    private void CheckTime()
    {
        lastTimeChecked = DateTime.Now;
    }
    
}
