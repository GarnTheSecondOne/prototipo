﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReferencesToScriptsForAds : MonoBehaviour {


    public bool pressed = false;
    public bool working = true;

    public void Start()
    {
        working = true;
        pressed = false;
    }

    public void OnlyOnce()
    {
        if (pressed == false && working)
        {
            CurrencyController.Instancia.GainCurrency(50, false);

            working = false;
            pressed = true;
            GetComponentInChildren<Text>().text = "50 Bits added!";
            GetComponent<Button>().interactable = false;
        }
    }

    private void OnEnable()
    {
        if (!EstadosDelJuego.Instancia.terminado)
        {
            working = false;

            GetComponent<Button>().interactable = false;
            GetComponentInChildren<Text>().text = "";
        }
        else
        {
            working = true;
            GetComponent<Button>().interactable = true;
            GetComponentInChildren<Text>().text = "Watch Ad for more Bits!";
        }
    }
}
