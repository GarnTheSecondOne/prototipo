﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrototipoNort
{
    public class MovimientoCamara : MonoBehaviour
    {
        private bool moviendo = true;
        private Vector3 posicionLlegada;

        public void Mover(Vector3 posicion)
        {
            moviendo = true;

            posicionLlegada = posicion; //crea una referencia de la posicion enviada
            Vector3 newPosition = new Vector3(posicion.x, posicion.y, transform.position.z); //Para que solo se mueva en x y y
            transform.position = (Vector3.Lerp(transform.position, newPosition, Time.deltaTime / 0.5f)); //Se mueve al lugar del nuevo menu

            if (Mathf.Abs(newPosition.x - transform.position.x) <= 0.1f && Mathf.Abs(newPosition.y - transform.position.y) <= 0.1f)
            {
                transform.position = newPosition;
            }

            if (transform.position == newPosition) //Si la posicion es igual a la nueva posición, se deja de mover
            {
                moviendo = false;
            }
        }

        private void LateUpdate()
        {
            if (moviendo)
            {
                Mover(posicionLlegada);
            }
        }
    }
}
