﻿using PrototipoNort;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoSingle : MonoBehaviour {

    public static void Mover(Vector3 posicion)
    {
        Camera.main.GetComponent<MovimientoCamara>().Mover(posicion);
    }
}
