﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {

    Canvas myCanvas;
    RenderMode originalRender;

	// Use this for initialization
	void Start () {
        myCanvas = this.GetComponent<Canvas>();
        originalRender = myCanvas.renderMode;

        CambiarRender();
	}

    private void CambiarRender()
    {
        myCanvas.renderMode = RenderMode.WorldSpace;
    }

    private void OnDestroy ()
    {
        myCanvas.renderMode = originalRender;
	}
}
