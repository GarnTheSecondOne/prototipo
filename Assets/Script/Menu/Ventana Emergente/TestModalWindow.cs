﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TestModalWindow : MonoBehaviour {

    private ModalPanel modalPanel;
    private PartCreator partCreator;
    private BaseObject reference;

    private void Awake()
    {
        modalPanel = ModalPanel.Instance();
        partCreator = PartCreator.Instance();
    }

    //Send to the modal panel to set up the buttons and functions to call
    public void TestTakeLeave()
    {
        TestPartCreation();
        modalPanel.NewObjectAppears(string.Format("You have obtained {0}!", reference.objectName), reference,() => { TestTakeOption(reference); }, TestLeaveOption);
    }

    private void TestPartCreation()
    {
        reference = partCreator.CreatedObject;
    }

    //These are wrapped into UnityActiovs
    private void TestTakeOption(BaseObject parte)
    {
        //Enviar baseobject
        InventoryManager.Instance().AddNewPart(parte);
    }

    private void TestLeaveOption()
    {
        Debug.Log("Not right now");
    }
}
