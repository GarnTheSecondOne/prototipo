﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BringToFront : MonoBehaviour {

    public bool onGame = false;

    private void OnEnable()
    {
        transform.SetAsLastSibling();
        if (!onGame)
        {
            transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, transform.position.z);
        }
    }
}
