﻿using UnityEngine;

public class DeactivateAtStart : MonoBehaviour {

    public static bool activated = false;

	void Awake ()
    {
        if (!activated)
        {
            gameObject.SetActive(false);
        }
	}

}
