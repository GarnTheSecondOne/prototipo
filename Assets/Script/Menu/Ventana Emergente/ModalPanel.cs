﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

//Giant space creature from outter space
public class ModalPanel : MonoBehaviour {

    [SerializeField]
    private Text message;
    [SerializeField]
    private Text extraMessage;
    [SerializeField]
    private Image iconImage;
    [SerializeField]
    private Button takeButton;
    [SerializeField]
    private Button leaveButton;

    [SerializeField]
    private GameObject modalPanelObject;

    [Space(10)]

    [SerializeField]
    private Text partName;
    [SerializeField]
    private Text partWear;
    [SerializeField]
    private Image partIcon;
    [SerializeField]
    private Text partDescription;
    [SerializeField]
    private Button useButton;
    [SerializeField]
    private Button returnButton;

    [SerializeField]
    private GameObject modalDescriptivePanelObject;


    private PartScrollList scrollPanel;

    private static ModalPanel modalPanel;

    public static ModalPanel Instance ()
    {
        if (!modalPanel)
        {
            modalPanel = FindObjectOfType(typeof(ModalPanel)) as ModalPanel;
            if (!modalPanel)
                Debug.LogError("There needs to be at least one GameObject with a ModalPanel script attached to it!");
        }

        return modalPanel;
    }

    public void Start()
    {
        scrollPanel = PartScrollList.Instance();
    }

    //Take/Leave: A string, a take event and a leave event
    public void NewObjectAppears(string message, BaseObject toUse, UnityAction takeEvent, UnityAction leaveEvent)
    {
        DeactivateAtStart.activated = true;
        modalPanelObject.SetActive(true);

        takeButton.onClick.RemoveAllListeners();
        takeButton.onClick.AddListener(takeEvent);
        takeButton.onClick.AddListener(CloseModalPanel);

        leaveButton.onClick.RemoveAllListeners();
        leaveButton.onClick.AddListener(leaveEvent);
        leaveButton.onClick.AddListener(CloseModalPanel);

        this.iconImage.gameObject.SetActive(true);
        this.iconImage.sprite = DiccionarySprites.GetSprite(toUse.icon + Tipo.moto); 

        this.message.text = message;

        this.extraMessage.gameObject.SetActive(false);
        takeButton.gameObject.SetActive(true);
        leaveButton.gameObject.SetActive(true);
    }

    public void PartSelected(BaseObject part, UnityAction useEvent)
    {
        DeactivateAtStart.activated = true;
        modalDescriptivePanelObject.SetActive(true);

        useButton.onClick.RemoveAllListeners();
        useButton.onClick.AddListener(useEvent);
        useButton.onClick.AddListener(CloseBothDescriptiveAndListPanels);

        returnButton.onClick.RemoveAllListeners();
        returnButton.onClick.AddListener(CloseModalDescriptivePanel);

        partName.text = part.objectName;
        partIcon.sprite = DiccionarySprites.GetSprite(part.icon + Tipo.moto);
        partWear.text = "Current Wear: " + part.desgastePropio.ToString("N2") + "%";

        if (part.GetType() == typeof(CoreObject))
        {
            CoreObject partCore = part as CoreObject;
            partDescription.text = string.Format("Wear Protection: {0} \nMax Velocity: {1} \nTurn Slowness: {2} \nShooting Slowness: {3} \nKnockback Protection: {4} ",
                partCore.desgaste.ToString(),
                partCore.maxVelocity.ToString(),
                partCore.disminucionAlGirar.ToString(),
                partCore.disminucionAlDisparar.ToString(),
                partCore.knockback.ToString());
        }
        if (part.GetType() == typeof(WeaponObject))
        {
            WeaponObject partWeapon = part as WeaponObject;
            partDescription.text = string.Format("Projectile number: {0} \nShooting Frequency: {1} \nShooting AoE: {2} \nShooting Scope: {3} \nShooting Nature: {4} ", 
                partWeapon.numProyectiles.ToString(),
                partWeapon.frecuenciaDisparo.ToString(),
                partWeapon.magnitudDisparo.ToString(),
                partWeapon.alcanceDisparo.ToString(),
                partWeapon.naturaleza.ToString());
        }
        if (part.GetType() == typeof(WheelsObject))
        {
            WheelsObject partWheel = part as WheelsObject;
            partDescription.text = string.Format("Acceleration: {0} \nTrace Length: {1}", 
                partWheel.aceleration.ToString(),
                partWheel.maxTrail.ToString());
        }

    }

    public void CloseModalDescriptivePanel()
    {
        modalDescriptivePanelObject.SetActive(false);
    }

    public void CloseBothDescriptiveAndListPanels()
    {
        scrollPanel.CloseScroll();
        modalDescriptivePanelObject.SetActive(false);
    }

    public void CloseModalPanel()
    {
        modalPanelObject.SetActive(false);
    }
}
