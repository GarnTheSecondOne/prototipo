﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseMyPanel : MonoBehaviour {

    public void ClickedOutside()
    {
        gameObject.SetActive(false);
    }
}
