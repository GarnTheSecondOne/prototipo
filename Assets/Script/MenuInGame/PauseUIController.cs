﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseUIController : MonoBehaviour {

    [SerializeField]
    private Button pauseButton;
    [SerializeField]
    private Image panelDeTexto;
    [SerializeField]
    private Text currencyText;
    [SerializeField]
    private Text hardCurrencyText;

    private bool pausado;

    public delegate void Pausando();
    public static event Pausando EnPausa;
    public static event Pausando EnDespausa;

	// Use this for initialization
	void Start ()
    {
        pausado = false;
        panelDeTexto.gameObject.SetActive(false);
    }
	
    public void Pausar()
    {
        if (!pausado)
        {
            panelDeTexto.gameObject.SetActive(true);
            pausado = true;
            EnPausa();
            StartCoroutine(EstarPasuasd());
        }
        else
        {
            Despausar();
        }
    }


    private void Despausar()
    {
        StopCoroutine(EstarPasuasd());
        pausado = false;
        panelDeTexto.gameObject.SetActive(false);
        EnDespausa();
    }

    private IEnumerator EstarPasuasd()
    {
        yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);

        currencyText.text = string.Format("Bits: {0}", "" + CurrencyController.Currency.ToString("0000"));
        hardCurrencyText.text = string.Format("Ultrabytes: {0}", "" + CurrencyController.CurrencyHard.ToString("0000"));

        if (Input.GetMouseButtonUp(0) && !IsPointerOverUIObject())
        {
            Despausar();
            StopAllCoroutines();
        }

        StartCoroutine(EstarPasuasd());
    }

    private bool IsPointerOverUIObject() //detecta si se ha tocado algo del ui o no
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
        };
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }



    //Eventos dentro de menu de pausa

    public void RegresarAMenu()
    {
        Despausar();
        StopAllCoroutines();

        SceneManager.LoadScene("MenuEscena");
    }

    public void VolverAJugar()
    {
        Despausar();
        StopAllCoroutines();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
