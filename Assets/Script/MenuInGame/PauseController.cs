﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {

    [SerializeField]
    private GameObject[] controles;

	// Use this for initialization
	private void Start () {
        PauseUIController.EnPausa += Pausar;
        PauseUIController.EnDespausa += Continuar;
	}

    private void OnDestroy()
    {
        PauseUIController.EnPausa -= Pausar;
        PauseUIController.EnDespausa -= Continuar;
    }
    
    private void Pausar()
    {
        for (int i = 0; i < controles.Length; i++)
        {
            controles[i].SetActive(false);

            Time.timeScale = 0;
        }
    }

    private void Continuar()
    {
        for (int i = 0; i < controles.Length; i++)
        {
            controles[i].SetActive(true);

            Time.timeScale = 1;
        }
    }
}
