﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonExistanbility : MonoBehaviour {

    [SerializeField]
    private Button[] botones;

    private void OnBecameVisible()
    {
        for (int i = 0; i < botones.Length; i++)
        {
            botones[i].gameObject.SetActive(true);
        }
    }

    private void OnBecameInvisible()
    {
        for (int i = 0; i < botones.Length; i++)
        {
            botones[i].gameObject.SetActive(false);
        }
    }

}
