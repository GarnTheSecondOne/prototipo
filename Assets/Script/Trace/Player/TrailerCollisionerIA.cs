﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailerCollisionerIA : MonoBehaviour
{
    #region Codigo Antiguo
    /*
    [SerializeField]
    int maxDistance = 50;
    [SerializeField]
    float minDistance = 0.5f;

    float tiempoParaIniciar = 5;
    bool funcionando;

    GameObject square;
    GameObject[] squareClon;
    GameObject[] pool;

    Vector3 lastPosition;
    int index;

    void Awake()
    {
        pool = GameObject.FindGameObjectsWithTag("LineaIA");
        for (int i = 0; i < pool.Length; i++)
        {
            pool[i].transform.position = new Vector3(900, -900, -900);
        }
    }

    void Start()
    {
        index = 0;
        lastPosition = transform.position;
        square = this.gameObject;
        squareClon = new GameObject[maxDistance];
        funcionando = false;


        for (int i = 0; i < squareClon.Length; i++)
        {
            //squareClon[i] = Instantiate(square, new Vector3(0, -10), Quaternion.identity) as GameObject;
            if (i < pool.Length)
            {
                squareClon[i] = pool[i];
                squareClon[i].transform.position = new Vector3(900, -900, -900);
            }
            else
            {
                print("Hay muy pocos trails de la IA");
                return;
            }
        }

        ReinicioController.enReinicio += Reinicio;
        BalasTrigger.EnDesaparecerLinea += Disappear;
    }


    void LateUpdate()
    {
        if (tiempoParaIniciar >= 0 && !funcionando)
        {
            tiempoParaIniciar -= Time.deltaTime;
        }

        if (tiempoParaIniciar < 0)
        {
            funcionando = true;
            tiempoParaIniciar = 0;
        }

        if (funcionando)
        {
            PonerColisionar();
        }

    }

    private void PonerColisionar()
    {
        Vector3 position = transform.position;
        float now = Time.time;

        if (position != lastPosition && (position - lastPosition).sqrMagnitude > minDistance * minDistance)
        {
            lastPosition = transform.position;

            squareClon[index].transform.position = position;
            //squareClon[index].transform.rotation = Quaternion.LookRotation(position - lastPosition, Vector3.up);

            index++;
            if (index >= squareClon.Length)
            {
                index = 0;
            }
        }
    }

    private void Reinicio()
    {
        funcionando = false;
        tiempoParaIniciar = 5;

        index = 0;
        lastPosition = transform.position;
        for (int i = 0; i < squareClon.Length; i++)
        {
            squareClon[i] = pool[i];
            squareClon[i].transform.position = new Vector3(900, -900, -900);
        }

        SendMessage("ReinicioJavastico");
    }

    private void Disappear(int indexClon)
    {
        squareClon[indexClon].transform.position = new Vector3(900, -900, -900);
    }

    */
    #endregion

        /// <summary>
        /// Codigo que maneja el trace de la IA.
        /// Funciona exactamente igual a TrailController.
        /// Se debe eliminar para el proyecto nuevo
        /// </summary>


    [SerializeField]
    int maxDistance = 50;
    [SerializeField]
    float minDistance = 0.5f;
    [SerializeField]
    float maxTime = 2f;
    [SerializeField]
    int id = 2;

    float tiempoParaIniciar = 5;
    bool funcionando;

    Vector3 lastPosition;
    int index;
    int turno;
    bool turnoDado = false;

    public delegate void AccionesDeTrail(Vector3 m_Position, Quaternion m_rotation, int quien, int cual);
    public static event AccionesDeTrail EnDesaparecerUno;
    public static event AccionesDeTrail EnPonerTrail;
    public static event AccionesDeTrail EnReiniciarTrail;
    public static event AccionesDeTrail EnDesaparecerLineaAtras;


    public delegate void AccionesExtras(int n, GameObject yo);
    public static event AccionesExtras EnReservarTrails;

    void Awake()
    {
        /*
        pool = GameObject.FindGameObjectsWithTag("Linea");
        for (int i = 0; i < pool.Length; i++)
        {
            pool[i].transform.position = new Vector3(900, -900, -900);
        }
        */
    }

    void Start()
    {
        index = 0;
        lastPosition = transform.position;
        funcionando = false;
        turnoDado = false;

        ReinicioController.enReinicio += Reinicio;
        BalasTrigger.EnDesaparecerLinea += Disappear;
        TrailPoolManager.EnAsignarTurno += AsignarTurno;
        BalasTrigger.EnDesaparecerAtrasLinea += DissapearVarious;

        turno = -1;
        StartCoroutine(EmpezarTarde());
    }

    private void OnDestroy()
    {
        ReinicioController.enReinicio -= Reinicio;
        BalasTrigger.EnDesaparecerLinea -= Disappear;
        TrailPoolManager.EnAsignarTurno -= AsignarTurno;
        BalasTrigger.EnDesaparecerAtrasLinea -= DissapearVarious;
    }

    IEnumerator EmpezarTarde()
    {
        yield return new WaitForSeconds(0.5f);
        EnReservarTrails(maxDistance, this.gameObject);
    }


    void LateUpdate()
    {
        //if (tiempoParaIniciar >= 0 && !funcionando)
        //{
        //    tiempoParaIniciar -= Time.deltaTime;
        //}

        if (!EstadosDelJuego.Instancia.Iniciando)
        {
            funcionando = true;
            tiempoParaIniciar = 0;
        }

        if (funcionando)
        {
            PonerLinea();
        }
    }

    void PonerLinea()
    {
        Vector3 position = transform.position;
        float now = Time.time;

        if (position != lastPosition && (position - lastPosition).sqrMagnitude > minDistance * minDistance)
        {
            lastPosition = transform.position;

            EnPonerTrail(lastPosition, Quaternion.identity, turno, index);
            index++;
            if (index >= maxDistance)
            {
                index = 0;
            }
        }
    }

    private void AsignarTurno(int _turno, GameObject deQuien)
    {
        if (!turnoDado && deQuien == this.gameObject)
        {
            if (TurnoSelector.PedirTurno(_turno))
            {
                turnoDado = true;
                id = _turno + 1;
                turno = _turno;
            }
        }
        //if (TrailCollisionerTest.evitarFallos == id)
        //{
        //    id++;
        //    TrailCollisionerTest.evitarFallos = 0;
        //    return;
        //}
        //if (TrailCollisionerTest.evitarFallos != id)
        //{
        //    TrailCollisionerTest.evitarFallos = id;
        //}

        //if ((id == (_turno + 1)) && turno == -1)
        //{
        //    turno = _turno;
        //}
    }

    private void Reinicio()
    {
        funcionando = false;
        tiempoParaIniciar = 5;

        EnReiniciarTrail(transform.position, transform.rotation, turno, maxDistance);

    }

    private void Disappear(int indexClon, int dueñoDeBala)
    {
        if (dueñoDeBala == turno)
        {
            EnDesaparecerUno(transform.position, transform.rotation, turno, indexClon);
        }
    }

    private void DissapearVarious(int indexClon, int dueñoDeBala)
    {
        if (dueñoDeBala == turno)
        {
            EnDesaparecerLineaAtras(transform.position, transform.rotation, turno, indexClon);
        }
    }

}

