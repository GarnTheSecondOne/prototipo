﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTrail : MonoBehaviour {

    /// <summary>
    /// Código incluido con cada instancia de trace. Usado para colisiones y manejado desde la pool
    /// </summary>

    Collider2D trigger; //el trigger de este objeto
    public int indice; //el indice de esta instancia
    public int dueño; //el dueño de esta instancia (winston)

    public delegate void OcurreCollision(int conCuantos);
    public static event OcurreCollision EnChocado;
    public static event OcurreCollision EnChoque;

    public delegate void OcurreColisionEspecial(Vector2 position, Quaternion velocity);
    public static event OcurreColisionEspecial EnChoqueJugador;
    public static event OcurreColisionEspecial EnChoqueEnemigo;
    //Cuando ocurren colisiones con el trace

    void Start ()
    {
        trigger = GetComponent<Collider2D>(); //el trigger es el componente collider 2d

        ChoqueDealer.enFinal += Detener; 
	}

    private void OnDestroy()
    {
        ChoqueDealer.enFinal -= Detener;
    }

    void OnTriggerEnter2D(Collider2D other) //si algo entra en el trigger
    {
        if (other.CompareTag("Player"))
        {
            if (!EstadosDelJuego.Instancia.invulnerable)
            {
                EnChoqueJugador(other.transform.position, (Quaternion.LookRotation(other.GetComponent<Rigidbody2D>().velocity, Vector3.forward)));
                EnChocado(1); //si es el jugador el que entró

            }
            else
            {
                other.gameObject.transform.position = other.gameObject.transform.position;
            }
        }
        if (other.CompareTag("Enemy"))
        {
            EnChoqueEnemigo(other.transform.position, (Quaternion.LookRotation(other.GetComponent<Rigidbody2D>().velocity, Vector3.forward)));
            EnChoque(1); //si es un enemigo el que entró
        }
    }

    public void Dissapear() //Para desaparecer esta instancia de trace después de un tiempo
    {
        transform.position = new Vector3(900, -900, -900); //lo envío a tomar por
        this.gameObject.SetActive(false); //lo desactica
    }

    private void Detener()
    {
        CancelInvoke(); //se cancela el invoke de Dissapear
    }
}
