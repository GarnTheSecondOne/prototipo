﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailCollisionerTest : MonoBehaviour {

    /// <summary>
    /// OH BOY. Este código se encarga de todo lo relacionado con poner y quitar los traces.
    /// Cada cierta distancia deja un Trace, accede a una pool para hacerlo
    /// Se asigna una ID para que no ocasione errores la pool única
    /// 
    /// El codigo que está comentado por completo está obsoleto o ya no se usa. Se deja en caso de que necesitemos,
    /// por ejemplo, hacer un trace "visible" rapidamente
    /// </summary>


    [SerializeField]
    int maxDistance = 50; //distancia máxima (aka traces maximos que se pueden poner)
    [SerializeField]
    float minDistance = 0.5f; //Distancia minima a la que se puede poner cada trace
    [SerializeField]
    float maxTime = 2f; //Tiempo máximo que dura el trace, creo que no se usa ya
    [SerializeField]
    int id = 1; //Identificador de este trailController

    public static int evitarFallos; //contador estático que evita que dos instancias tengan el mismo id

    float tiempoParaIniciar = 5; //cuanto tarda en iniciar el trace
    bool funcionando; //si este tracer está funcionando


    Vector3 lastPosition; //ultima posición en la que se puso un trace      
    int index; //indicador de cual trace se pone
    int turno; //relacionado con el id, usado para distinguir cuales traces poner de la pool
    bool turnoDado = false;

    //private Color colorTrail; 

    public delegate void AccionesDeTrail(Vector3 m_Position, Quaternion m_rotation, int quien, int cual);
    public static event AccionesDeTrail EnDesaparecerUno;
    public static event AccionesDeTrail EnPonerTrail;
    public static event AccionesDeTrail EnReiniciarTrail;
    public static event AccionesDeTrail EnDesaparecerLineaAtras;
    //Eventos que se envian a la pool para realizar acciones con el trace


    public delegate void AccionesExtras(int n, GameObject yo);
    public static event AccionesExtras EnReservarTrails;
    //evento enviado a la pool, una sola vez, para que cada instancia tenga su propia pool reservada


  

    public void SetTrail(int maxDistancia)
    {
        maxDistance = maxDistancia; //Enviado desde la selección de vehiculo, para settear la distancia del trace

        //Iniciar();
    }

	void Start ()
    {
        turnoDado = false;
        evitarFallos = 0; //En todas las instancias se inicia en 0

        index = 0; 
        lastPosition = transform.position; //la ultima posición empieza como la posición en la cual inicia el winston
        //square = this.gameObject;
        //squareClon = new GameObject[maxDistance];
        funcionando = false; //inicia sin funcionar


        ReinicioController.enReinicio += Reinicio;
        BalasTrigger.EnDesaparecerLinea += Disappear;
        TrailPoolManager.EnAsignarTurno += AsignarTurno;
        BalasTrigger.EnDesaparecerAtrasLinea += DissapearVarious;
        //TrailPoolManager.EnPedirAsignacion += ReservarTrailsPeroYa;

       // puntos = new List<Vector3>();
        //linea = GetComponent<LineRenderer>();
        //linea.startWidth = grosor; linea.endWidth = grosor;
        //colorTrail = linea.GetComponent<LineRenderer>().material.color;

        turno = -1; //se inicia en -1 para que se eviten fallos de reservar de pool
        StartCoroutine(EmpezarTarde()); 
    }

    private void OnDestroy()
    {
        ReinicioController.enReinicio -= Reinicio;
        BalasTrigger.EnDesaparecerLinea -= Disappear;
        TrailPoolManager.EnAsignarTurno -= AsignarTurno;
        BalasTrigger.EnDesaparecerAtrasLinea -= DissapearVarious;
        //TrailPoolManager.EnPedirAsignacion -= ReservarTrailsPeroYa;
    }

    IEnumerator EmpezarTarde() //Creada para evitar errores por selección de vehiculo. Se demora un poco para llamar la pool para dejar que se cree
    {
        yield return new WaitForSeconds(0.5f);
        EnReservarTrails(maxDistance, this.gameObject); //Enviado a la pool, reserva una pool propia de la distancia máxima
    }

    public void ReservarTrailsPeroYa()
    {
        if (turno == -1)
        {
            EnReservarTrails(maxDistance, this.gameObject);
        }
    }


    void LateUpdate ()
    {
        //if (tiempoParaIniciar >= 0 && !funcionando)
        //{
        //    tiempoParaIniciar -= Time.deltaTime; //La mejor y unica forma existente de esperar un tiempo
        //}

        if (!EstadosDelJuego.Instancia.Iniciando)
        {
            funcionando = true; //ahora funciona el winston
            //tiempoParaIniciar = 0;
        }
        if (EstadosDelJuego.Instancia.invulnerable)
        {
            if (!IsInvoking("QuitarInvulnerabilidad"))
            {
                Invoke("QuitarInvulnerabilidad", 2);
            }
            funcionando = false;
        }

        if (funcionando)
        {
            PonerLinea(); //Pone un trace (duh)
        }
	}

    private void QuitarInvulnerabilidad()
    {
        EstadosDelJuego.Instancia.invulnerable = false;
    }

    private void PonerLinea()
    {
        Vector3 position = transform.position; //la posición en la que se va a poner el trace
        //float now = Time.time;

        if (position != lastPosition && (position - lastPosition).sqrMagnitude > minDistance * minDistance) //si la posición no es igual a la ultima posicio, y la distancia entre estas (al cuadrado) es mayor que la distancia minima al cuadrado
        {

            lastPosition = transform.position; //la ultima posición es ahora la posición actual

            #region Visible

            //if (linea.GetComponent<LineRenderer>().material.color.a <= 0)
            //{
            //    linea.GetComponent<LineRenderer>().material.color = colorTrail;
            //}

            //Vector3 posicion = transform.position;

            //if (puntos == null)
            //{
            //    puntos = new List<Vector3>();
            //}
            //puntos.Insert(0, posicion);

            //if (puntos.Count <= maxDistance)
            //{
            //    linea.positionCount = puntos.Count;
            //    linea.SetPosition(contador, puntos[0]);
            //    contador += 1;
            //}
            //else
            //{

            //    for (int i = 0; i < linea.positionCount; i++)
            //    {
            //        if (i + 1 < linea.positionCount)
            //        {
            //            linea.SetPosition(i, linea.GetPosition(i + 1));
            //        }
            //        else
            //        {
            //            linea.SetPosition(i,puntos[0]);
            //        }
            //    }
            //}
            #endregion

            EnPonerTrail(lastPosition, GetComponentInParent<Transform>().rotation, turno, index); //se envia a la pool, con la rotación del vehiculo, el turno y el index de la que se debe poner
            index++; //se aumenta el indice
            if (index >= maxDistance)
            {
                index = 0; //si el indice supera la distancia maxima, se vuelve a 0
            }
        }
    }

    private void AsignarTurno(int _turno, GameObject deQuien) //Asigna los turnos a cada instancia del winston, llamado desde la pool
    {
        if (!turnoDado && deQuien == this.gameObject)
        {
            if (TurnoSelector.PedirTurno(_turno))
            {
                turnoDado = true;
                id = _turno + 1;
                turno = _turno;
            }
        }
        //if (evitarFallos == id) //Si evitar fallos es igual a esta instancia, se aumenta el id.
        //{
        //    id++;
        //    evitarFallos = 0;
        //    return;
        //}
        //if (evitarFallos != id) //si evitar fallos es diferente de esta instancia, se vuelve del mismo valor del id
        //{
        //    evitarFallos = id;
        //}
        //if ((id == (_turno + 1)) && turno == -1) //si el id es igual al turno (que debería ser (id - 1)), y el turno no se ha cambiado antes 
        //{
        //    turno = _turno; //el turno de esta instancia es el que fue enviado por la pool
        //}
    }

    private void Reinicio() //Para reiniciar todo el trace
    {
        //DesaparecerVisible();
        funcionando = false; //deja de funcionar
        tiempoParaIniciar = 5; //vuelve a esperar

        /*
        index = 0;
        lastPosition = transform.position;
        for (int i = 0; i < squareClon.Length; i++)
        {
            squareClon[i] = pool[i];
            squareClon[i].transform.position = new Vector3(900, -900, -900);
        }
        */

        EnReiniciarTrail(transform.position, transform.rotation, turno, maxDistance); //se reinicia el trace desde la pool (lo importante de este envio de evento son el turno y la distancia)

        //SendMessage("ReinicioJavastico");
    }

    //private void DesaparecerVisible()
    //{
    //    //linea.GetComponent<LineRenderer>().material.color = new Color(0, 0, 0, 0f);
    //    puntos.Clear();
    //    contador = 0;
    //    //linea.startWidth = grosor; linea.endWidth = grosor;
    //}

    private void Disappear(int indexClon, int dueñoDeBala) //Cuando se quiere desaparecer una parte de un trace
    {
        //squareClon[indexClon].transform.position = new Vector3(900, -900, -900);

        if (dueñoDeBala == turno) //si el turno que la bala detectó es igual al turno de esta instancia
        {
            EnDesaparecerUno(transform.position, transform.rotation, turno, indexClon); //se desaparece el trace indicado
            //CancelInvoke();
            //Invoke("LanzarRayos", 0.01f);
        }
    }

    private void DissapearVarious(int indexClon, int dueñoDeBala) //cuando se quiere desaparecer desde una parte del trace
    {
        if (dueñoDeBala == turno)
        {
            EnDesaparecerLineaAtras(transform.position, transform.rotation, turno, indexClon); //lo que cambia es el evento que se envía
        }
    }



}
