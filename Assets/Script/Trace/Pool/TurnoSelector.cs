﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnoSelector {

    private static int turnoIndex = 0;

    public static bool PedirTurno(int turno)
    {
        if (turno == turnoIndex)
        {
            turnoIndex++;
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void ReiniciarTurnos()
    {
        turnoIndex = 0;
    }
}
