﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailPoolManager : MonoBehaviour {

    /// <summary>
    /// 
    /// </summary>

    [HideInInspector]
    public GameObject[] trails; //Todos los traces

    private int index; //el indice actual de la pool
    private int indexIndex; //el indice del grupo de traces actual
    private int indiceAEliminar; //el indice actual del trace que debe desaparecer (cuando se quita el trace hacia atrás)
    private bool accionarTrails; //indica si los traces deben funcionar

    public delegate void OcurrenciasDelPool(int turno, GameObject aQuien);
    public static event OcurrenciasDelPool EnAsignarTurno;
    //Se envia a los winston, para asignar turnos

    public delegate void MasOcurrencias();
    public static event MasOcurrencias EnPedirAsignacion;

    //public delegate void OcurrenciasDelTrail(Vector3 transform);
    //public static event OcurrenciasDelTrail EnDesdibujar;

    public class IndexReservado //Clase usada para que cada winston reserve su propio trace
    {
        public int[] reserva; //el indice de la reserva de traces
        public int ultimoPuesto; //el indice del ultimo trace puesto
    }

    private IndexReservado[] indexReservado; //se crea un array de la clase anterior

    void Awake ()
    {
        index = 0; //actualmente el indice es 0
        indexIndex = 0; //el primer grupo es el 0
        trails = GameObject.FindGameObjectsWithTag("Linea"); //se encuentran todos los trails con el tag "Linea"
        if (trails == null)
        {
            Debug.Log("Por favor agregar tag 'Linea' a trails en el pool"); 
        }

        IniciarTrails(); //Se los desaparece a todos por completo del mapa y se les agrega un indice individual a cada uno
    }

    private void OnDestroy()
    {
        TrailCollisionerTest.EnReservarTrails -= ReservarTrails;
        TrailCollisionerTest.EnPonerTrail -= PonerUnTrail;
        TrailCollisionerTest.EnReiniciarTrail -= ReiniciarTrail;
        TrailCollisionerTest.EnDesaparecerUno -= DesaparecerUno;
        TrailCollisionerTest.EnDesaparecerLineaAtras -= DesaparecerHaciaAtras;

        TrailerCollisionerIA.EnReservarTrails -= ReservarTrails;
        TrailerCollisionerIA.EnPonerTrail -= PonerUnTrail;
        TrailerCollisionerIA.EnReiniciarTrail -= ReiniciarTrail;
        TrailerCollisionerIA.EnDesaparecerUno -= DesaparecerUno;
        TrailerCollisionerIA.EnDesaparecerLineaAtras -= DesaparecerHaciaAtras;

        trails = null;
        TurnoSelector.ReiniciarTurnos();
    }

    void Start()
    {
        indexReservado = new IndexReservado[8]; //En el prototipo, se inicia el array de tamaño 4
        for (int i = 0; i < indexReservado.Length; i++)
        {
            indexReservado[i] = new IndexReservado(); //Esto es para inicializar cada array de clase
        }

        accionarTrails = true; //en un principio, el codigo debe funcionar

        TrailCollisionerTest.EnReservarTrails += ReservarTrails;
        TrailCollisionerTest.EnPonerTrail += PonerUnTrail;
        TrailCollisionerTest.EnReiniciarTrail += ReiniciarTrail;
        TrailCollisionerTest.EnDesaparecerUno += DesaparecerUno;
        TrailCollisionerTest.EnDesaparecerLineaAtras += DesaparecerHaciaAtras;

        TrailerCollisionerIA.EnReservarTrails += ReservarTrails;
        TrailerCollisionerIA.EnPonerTrail += PonerUnTrail;
        TrailerCollisionerIA.EnReiniciarTrail += ReiniciarTrail;
        TrailerCollisionerIA.EnDesaparecerUno += DesaparecerUno;
        TrailerCollisionerIA.EnDesaparecerLineaAtras += DesaparecerHaciaAtras;

        ChoqueDealer.enFinal += DejarTodo;
    }
	



    private void IniciarTrails() //Para darle valores y posiciones iniciales
    {
        for (int i = 0; i < trails.Length; i++)
        {
            trails[i].transform.position = new Vector3(900, -900, -900);
            trails[i].gameObject.SetActive(false);
            trails[i].GetComponent<TriggerTrail>().indice = i; //se les da un primer indice, igual a i
        }
    }

    private void ReservarTrails(int cuantos, GameObject quien) //le llega cuantos trails quieren reservar y envía un evento para que los puedan reservar
    {
        if (indexReservado[indexIndex].reserva == null)
        {
            indexReservado[indexIndex].reserva = new int[cuantos]; //al indice de grupo actual se le reserva el numero pedido
        }


        for (int i = 0; i < cuantos; i++)
        {
            indexReservado[indexIndex].reserva[i] = index; //a la reserva de este grupo se le asigna el indice actual de la pool
            trails[index].GetComponent<TriggerTrail>().dueño = indexIndex; //a cada trace se le pone el numero del dueño como el indice de grupo
            trails[index].GetComponent<TriggerTrail>().indice = i; //se le cambia el indice de cada trace para que cuadre con el i del for
            index++;
            if (index >= trails.Length)
            {
                index = 0;
                Debug.Log("Muy pocos trails");
            }
        }
        //asignador.turno = indexIndex;
        EnAsignarTurno(indexIndex, quien); //Se envía un evento a cada winston dandole este indice de grupo como turno


        indexIndex++; //se aumenta el turno de grupo
        if (indexIndex >= indexReservado.Length)
        {
            indexIndex = 0; //Si se supera de la longitud de "jugadores"
            Debug.Log("max players");
        }
    }

    private void PonerUnTrail(Vector3 lugar, Quaternion rotation, int quien, int cual) //Se pone un trace en la posición deseada con la rotación deseada
    {
        if (!accionarTrails)
        {
            return; //si no están funcionando, que se devuelva
        }
        if (quien < 0)
        {
            return;
        }

        trails[indexReservado[quien].reserva[cual]].gameObject.SetActive(true); //se activa el trace de un quien (el turno de quien lo pide) y el cual (el indice que indica debe ponerse)
        trails[indexReservado[quien].reserva[cual]].GetComponent<TriggerTrail>().CancelInvoke(); //se cancela el invoke de dissapear
        trails[indexReservado[quien].reserva[cual]].GetComponent<TriggerTrail>().Invoke("Dissapear", 3); //Se invoca el dissapear en 3 segundos desde que se puedo el trace
        trails[indexReservado[quien].reserva[cual]].gameObject.transform.SetPositionAndRotation(lugar, rotation); //se le asigna un lugar y una rotación
        indexReservado[quien].ultimoPuesto = cual; //el ultimo puesto de este grupo fue el actual
    }

    private void ReiniciarTrail(Vector3 lugar, Quaternion rotation, int quien, int cuantos) //Se reinician todos los traces
    {
        accionarTrails = true; 
        for (int i = 0; i < indexReservado[quien].reserva.Length; i++)
        {
            trails[indexReservado[quien].reserva[i]].transform.position = new Vector3(900, -900, -900); //Se envian a la pm
            trails[i].gameObject.SetActive(false); //se desactivan
        }
        IniciarTrails();
    }

    private void DejarTodo() //Desactivar las acciones del trace
    {
        accionarTrails = false;
    }

    private void DesaparecerUno(Vector3 lugar, Quaternion rotation, int quien, int cual) //Cuando se quiere desaparecer un pedazo del trace
    {

        if (!accionarTrails)
        {
            return;
        }
        //Debug.Log("Quien: " + quien + "\nCual: " + cual);
        trails[indexReservado[quien].reserva[cual]].transform.position = new Vector3(900, -900, -900);
        trails[indexReservado[quien].reserva[cual]].gameObject.SetActive(false);
    }

    private void DesaparecerHaciaAtras(Vector3 lugar, Quaternion rotation, int quien, int cual) //Cuando se quiere desaparecer desde uno hacia atrás
    {
        if (!accionarTrails)
        {
            return;
        }

        trails[indexReservado[quien].reserva[cual]].transform.position = new Vector3(900, -900, -900); //el actual se envia hacia el vacio
        trails[indexReservado[quien].reserva[cual]].gameObject.SetActive(false);

        indiceAEliminar = cual; //el que se va a eliminar es el actual
        for(int i = 0; i < indexReservado[quien].reserva.Length; i++) 
        {
            trails[indexReservado[quien].reserva[indiceAEliminar]].transform.position = new Vector3(900, -900, -900); //se eliminar el que "se va a eliminar"
            trails[indexReservado[quien].reserva[indiceAEliminar]].gameObject.SetActive(false);
            indiceAEliminar--; //Se baja el indice del que se va a eliminar
            if (indiceAEliminar == 0) //si ese indice llega a 0, se convierte en el ultimo indice de este grupo
            {
                indiceAEliminar = indexReservado[quien].reserva.Length - 1;
            }
            if ((indiceAEliminar == indexReservado[quien].ultimoPuesto)) //si el indice a eliminar llega a ser igual al ultimo trace puesto, se cancela el for
            {
                break;
            }
        }
    }
}
