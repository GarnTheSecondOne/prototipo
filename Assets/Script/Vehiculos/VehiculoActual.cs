﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehiculoActual {

    public static Tipo type;
    public static CoreObject core;
    public static WheelsObject wheels;
    public static WeaponObject weapon;
    public static int numDeVehiculos;

    private static VehiculoActual instacia = null;

    public static VehiculoActual Instancia
    {
        get
        {
            if (instacia == null)
            {
                instacia = new VehiculoActual();
            }
            return instacia;
        }
    }


    public static void PartesActuales(CoreObject cores, WeaponObject weapons, WheelsObject wheelss)
    {
        core = cores;
        weapon = weapons;
        wheels = wheelss;
    }

    //public static void PartesActuales(CoreObject cores)
    //{
    //    core = cores;
    //}

    //public static void PartesActuales(WeaponObject weapons)
    //{
    //    weapon = weapons;
    //}

    //public static void PartesActuales(WheelsObject wheelss)
    //{
    //    wheels = wheelss;
    //}

    //public CoreObject GetCore
    //{
    //    get
    //    {
    //        return core;
    //    }
    //}

    //public WeaponObject GetWeapon
    //{
    //    get
    //    {
    //        return weapon;
    //    }
    //} 

    //public WheelsObject GetWheels
    //{
    //    get
    //    {
    //        return wheels;
    //    }
    //}
}
