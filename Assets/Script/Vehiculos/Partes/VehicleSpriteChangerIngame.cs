﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSpriteChangerIngame : MonoBehaviour {

    [SerializeField]
    private SpriteRenderer wheelsIcon;
    [SerializeField]
    private SpriteRenderer coreIcon;
    [SerializeField]
    private SpriteRenderer weaponIcon;
    [SerializeField]
    private SpriteRenderer pilotIcon;

    public void ChangeSprite(VehicleObject vehiculoActual)
    {
        if (vehiculoActual == null)
        {
            Debug.LogError("Sending null to sprite changer ingame");
        }
        else
        {
            wheelsIcon.sprite = DiccionarySprites.GetSprite(vehiculoActual.wheels.icon + vehiculoActual.type);
            coreIcon.sprite = DiccionarySprites.GetSprite(vehiculoActual.core.icon + vehiculoActual.type);
            weaponIcon.sprite = DiccionarySprites.GetSprite(vehiculoActual.weapon.icon + vehiculoActual.type);
        }
    }
}
