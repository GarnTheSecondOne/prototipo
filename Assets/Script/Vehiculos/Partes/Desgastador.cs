﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desgastador : MonoBehaviour {

    [SerializeField]
    private float desgasteParaPerdida = 15f;
    [SerializeField]
    private float desgasteParaGanada = 5f;

    private CoreObject coreActual;
    private WheelsObject wheelsActual;
    private WeaponObject weaponActual;

    private BaseObject[] objetosActuales = new BaseObject[3]; 

    private float desgasteReductor = 1;


	void Start ()
    {
        ChoqueDealer.enPerder += DesgastarEnPerdida;
        ChoqueDealer.enGanar += DesgastarEnGanada;
	}

    public void SetPartesActuales(CoreObject core, WheelsObject wheels, WeaponObject weapon)
    {
        objetosActuales[0] = core;
        objetosActuales[1] = wheels;
        objetosActuales[2] = weapon;
    }

    public void SetDesgaste(float desgaste)
    {
        desgasteReductor = desgaste;
    }
	

    private void DesgastarEnPerdida()
    {
        for (int i = 0; i < objetosActuales.Length; i++)
        {
            if (objetosActuales[i].id != 0)
            {
                objetosActuales[i].desgastePropio += (desgasteParaPerdida * desgasteReductor);
                Debug.Log(string.Format("{0} has {1} points of damage", objetosActuales[i], objetosActuales[i].desgastePropio));
                if (objetosActuales[i].desgastePropio >= 100)
                {
                    ObjetoPerdido(objetosActuales[i]);
                }
            }
        }
    }

    private void DesgastarEnGanada()
    {
        for (int i = 0; i < objetosActuales.Length; i++)
        {
            if (objetosActuales[i].id != 0)
            {
                objetosActuales[i].desgastePropio += (desgasteParaGanada * desgasteReductor);
                Debug.Log(string.Format("{0} has {1} points of damage", objetosActuales[i], objetosActuales[i].desgastePropio));
                if (objetosActuales[i].desgastePropio >= 100)
                {
                    ObjetoPerdido(objetosActuales[i]);
                }
            }
        }
    }

    private void ObjetoPerdido(BaseObject baseObject)
    {
        Debug.Log(string.Format("{0} has been destroyed", baseObject));
        Reparar(); //De momento
    }

    private void Reparar()
    {
        for (int i = 0; i < objetosActuales.Length; i++)
        {
            objetosActuales[i].desgastePropio = 0;
        }
    }

    private void OnDestroy()
    {
        ChoqueDealer.enPerder -= DesgastarEnPerdida;
        ChoqueDealer.enGanar -= DesgastarEnGanada;
    }

}

