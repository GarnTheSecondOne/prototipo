﻿using System;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SeleccionarVehiculo : MonoBehaviour {

    /// <summary>
    /// Codigo para cambiar los parametros de diferentes scripts para asemejarse al comportamiento de las partes
    /// Tengo pereza y estoy cansado, luego comento todo esto bien
    /// </summary>

    //public enum tipo
    //{
    //    carroAlfa = 1,
    //    carroBeta = 2,
    //    moto = 0
    //}

    [Header("Selección de Vehiculo")]
    [SerializeField]
    private VehicleObject[] vehiculosDePrueba;

    [HideInInspector]
    public VehicleObject[] vehicleUsado;
    //[SerializeField]
    //private Tipo tipoDeVehiculo;
    //[SerializeField]
    private bool seleccionManual = false;

    private int vehiculoActual = 0;
    private bool cambioDeVehiculo = false;

    [SerializeField]
    private GameObject carro;
    [SerializeField]
    private GameObject carroBeta;
    [SerializeField]
    private GameObject moto;

    private GameObject[] clonDeVehiculo;

    private VirajeController viraje;
    private MovementPlayer movement;
    private TrailCollisionerTest[] trail;
    private DisparadorScript disparador;
    private Desgastador desgastador;
    private VehicleSpriteChangerIngame vehicleSpriteChanger;

    void Awake () //Carga los recursos de moto y carro
    {
        //carro = Resources.Load("TestCarro") as GameObject;
        //carroBeta = Resources.Load("TestCarroBeta") as GameObject;
        //moto = Resources.Load("TestMoto") as GameObject;
        cambioDeVehiculo = false;
        vehiculoActual = 0;

        carro.SetActive(false);
        carroBeta.SetActive(false);
        moto.SetActive(false);

        if (!seleccionManual)
        {
            SetDesdeControlador();
        }
        else if (seleccionManual)
        {
            SetManual();
        }
        EstadosDelJuego.Instancia.invulnerable = false;
        SetVehiculo();
	}

    private void Start()
    {
        ScoreController.EnMenorVida += CambioDeVehiculo;
        ReinicioController.enReinicio += NuevoVehiculo;
    }
    private void OnDestroy()
    {
        ScoreController.EnMenorVida -= CambioDeVehiculo;
        ReinicioController.enReinicio -= NuevoVehiculo;
    }

    private void SetManual()
    {
        VehiculoActual.numDeVehiculos = vehicleUsado.Length;
        clonDeVehiculo = new GameObject[vehicleUsado.Length];


    }

    private void SetDesdeControlador()
    {
        vehicleUsado = new VehicleObject[3];
        VehiculoActual.numDeVehiculos = vehicleUsado.Length;

        //try
        //{
        //    vehicleUsado[vehiculoActual] = ControladorMotoTemporal.vehicleAUsar[vehiculoActual];
        //    if (vehicleUsado[vehiculoActual].funcionando)
        //    {
        //        clonDeVehiculo = new GameObject[vehicleUsado.Length];
        //    }

        //}
        //catch (NullReferenceException)
        //{
        //Debug.Log("Partes no encontradas, Usando datos iniciales");
        for (int i = 0; i < vehicleUsado.Length; i++)
        {
            vehicleUsado[i] = InventoryManager.Instance().inventory.vehiculo[i];
        }
            ControladorMotoTemporal.vehicleAUsar = vehicleUsado;
            if (vehicleUsado[vehiculoActual].funcionando)
            {
                clonDeVehiculo = new GameObject[vehicleUsado.Length];
            }
        //}
    }

    private void SetVehiculo() //Instancia el vehiculo según el tipo seleccionado
    {

        switch (vehicleUsado[vehiculoActual].type)
        {
            case Tipo.carroAlfa:
                //clonDeVehiculo[vehiculoActual] = Instantiate(carro, transform.position, transform.rotation);
                clonDeVehiculo[vehiculoActual] = carro;
                clonDeVehiculo[vehiculoActual].transform.position = transform.position;
                clonDeVehiculo[vehiculoActual].transform.rotation = transform.rotation;
                break;

            case Tipo.carroBeta:
                // clonDeVehiculo[vehiculoActual] = Instantiate(carroBeta, transform.position, transform.rotation);
                clonDeVehiculo[vehiculoActual] = carroBeta;
                clonDeVehiculo[vehiculoActual].transform.position = transform.position;
                clonDeVehiculo[vehiculoActual].transform.rotation = transform.rotation;
                break;

            case Tipo.moto:
                //clonDeVehiculo[vehiculoActual] = Instantiate(moto, transform.position, transform.rotation);
                clonDeVehiculo[vehiculoActual] = moto;
                clonDeVehiculo[vehiculoActual].transform.position = transform.position;
                clonDeVehiculo[vehiculoActual].transform.rotation = transform.rotation;
                break;


            default:
                Debug.Log("Joder tio, algo falla aqui");
                break;
        }
        InstantiateOthers();
        SetPropiedades();
    }

    private void InstantiateOthers()
    {
        for (int i = 1; i < vehicleUsado.Length; i++)
        {
            if (!seleccionManual)
            {
                //vehicleUsado[i] = ControladorMotoTemporal.vehicleAUsar[i];
            }

            if (vehicleUsado[i].funcionando)
            {

                switch (vehicleUsado[i].type)
                {
                    case Tipo.carroAlfa:
                        //clonDeVehiculo[vehiculoActual] = Instantiate(carro, transform.position, transform.rotation);
                        clonDeVehiculo[i] = carro;
                        clonDeVehiculo[i].transform.position = transform.position;
                        clonDeVehiculo[i].transform.rotation = transform.rotation;
                        break;

                    case Tipo.carroBeta:
                        // clonDeVehiculo[vehiculoActual] = Instantiate(carroBeta, transform.position, transform.rotation);
                        clonDeVehiculo[i] = carroBeta;
                        clonDeVehiculo[i].transform.position = transform.position;
                        clonDeVehiculo[i].transform.rotation = transform.rotation;
                        break;

                    case Tipo.moto:
                        //clonDeVehiculo[vehiculoActual] = Instantiate(moto, transform.position, transform.rotation);
                        clonDeVehiculo[i] = moto;
                        clonDeVehiculo[i].transform.position = transform.position;
                        clonDeVehiculo[i].transform.rotation = transform.rotation;
                        break;


                    default:
                        Debug.Log("Joder tio, algo falla aqui");
                        break;
                }

                clonDeVehiculo[i].transform.position = new Vector3(-900, -900, -900);
                vehiculoActual = i;
                SetPropiedades();
                clonDeVehiculo[i].SetActive(false);
            }
        }
        vehiculoActual = 0;
    }

    private void SetPropiedades() //Pone las propiedades al vehiculo
    {
        if (vehiculoActual < 1)
        {
            ActivateCar();
        }

        viraje = clonDeVehiculo[vehiculoActual].GetComponent<VirajeController>();
        movement = clonDeVehiculo[vehiculoActual].GetComponent<MovementPlayer>();
        disparador = clonDeVehiculo[vehiculoActual].GetComponentInChildren<DisparadorScript>();
        desgastador = GetComponent<Desgastador>();
        vehicleSpriteChanger = clonDeVehiculo[vehiculoActual].GetComponentInChildren<VehicleSpriteChangerIngame>();

        VehiculoActual.type = vehicleUsado[vehiculoActual].type;
        switch (vehicleUsado[vehiculoActual].type)
        {
            case Tipo.carroAlfa:
                trail = new TrailCollisionerTest[2];
                viraje.SetParameters(VirajeController.TipoDePivote.delantero);
                movement.SetParameters(vehicleUsado[vehiculoActual].wheels.aceleration/2, vehicleUsado[vehiculoActual].core.maxVelocity, vehicleUsado[vehiculoActual].core.disminucionAlDisparar, vehicleUsado[vehiculoActual].core.disminucionAlGirar, vehicleUsado[vehiculoActual].core.knockback);
                disparador.SetDisparos(vehicleUsado[vehiculoActual].weapon.frecuenciaDisparo, vehicleUsado[vehiculoActual].weapon.alcanceDisparo, vehicleUsado[vehiculoActual].weapon.magnitudDisparo, vehicleUsado[vehiculoActual].weapon.naturaleza, vehicleUsado[vehiculoActual].weapon.numProyectiles);
                for (int i = 0; i < trail.Length; i++)
                {
                    trail[i] = clonDeVehiculo[vehiculoActual].GetComponentsInChildren<TrailCollisionerTest>()[i];
                    trail[i].SetTrail(vehicleUsado[vehiculoActual].wheels.maxTrail / 2);
                }


                //clonDeVehiculo[vehiculoActual].GetComponentInChildren<VehicleSpriteChanger>().ChangeSprite(0); //Cambio nuevo


                break;


            case Tipo.carroBeta:
                trail = new TrailCollisionerTest[2];
                viraje.SetParameters(VirajeController.TipoDePivote.trasero);
                movement.SetParameters(
                    vehicleUsado[vehiculoActual].wheels.aceleration / 2, 
                    vehicleUsado[vehiculoActual].core.maxVelocity, 
                    vehicleUsado[vehiculoActual].core.disminucionAlDisparar, 
                    vehicleUsado[vehiculoActual].core.disminucionAlGirar, 
                    vehicleUsado[vehiculoActual].core.knockback);
                disparador.SetDisparos(vehicleUsado[vehiculoActual].weapon.frecuenciaDisparo, vehicleUsado[vehiculoActual].weapon.alcanceDisparo, vehicleUsado[vehiculoActual].weapon.magnitudDisparo, vehicleUsado[vehiculoActual].weapon.naturaleza, vehicleUsado[vehiculoActual].weapon.numProyectiles);
                for (int i = 0; i < trail.Length; i++)
                {
                    trail[i] = clonDeVehiculo[vehiculoActual].GetComponentsInChildren<TrailCollisionerTest>()[i];
                    trail[i].SetTrail(vehicleUsado[vehiculoActual].wheels.maxTrail / 2);
                }


                //clonDeVehiculo[vehiculoActual].GetComponentInChildren<VehicleSpriteChanger>().ChangeSprite(1); //Cambio nuevo


                break;

            case Tipo.moto:
                trail = new TrailCollisionerTest[1];
                viraje.SetParameters(VirajeController.TipoDePivote.normal);
                movement.SetParameters(vehicleUsado[vehiculoActual].wheels.aceleration, vehicleUsado[vehiculoActual].core.maxVelocity, vehicleUsado[vehiculoActual].core.disminucionAlDisparar, vehicleUsado[vehiculoActual].core.disminucionAlGirar, vehicleUsado[vehiculoActual].core.knockback);
                disparador.SetDisparos(vehicleUsado[vehiculoActual].weapon.frecuenciaDisparo, vehicleUsado[vehiculoActual].weapon.alcanceDisparo, vehicleUsado[vehiculoActual].weapon.magnitudDisparo, vehicleUsado[vehiculoActual].weapon.naturaleza, vehicleUsado[vehiculoActual].weapon.numProyectiles);
                trail[0] = clonDeVehiculo[vehiculoActual].GetComponentInChildren<TrailCollisionerTest>();
                trail[0].SetTrail(vehicleUsado[vehiculoActual].wheels.maxTrail);
                break;

            default:
                Debug.Log("Joder tio, algo falla aun más aqui");
                break;
        }

        desgastador.SetPartesActuales(vehicleUsado[vehiculoActual].core, vehicleUsado[vehiculoActual].wheels, vehicleUsado[vehiculoActual].weapon);
        desgastador.SetDesgaste(vehicleUsado[vehiculoActual].core.desgaste);

        vehicleSpriteChanger.ChangeSprite(vehicleUsado[vehiculoActual]);
    }

	public float getMaxVel() {
		return vehicleUsado[vehiculoActual].core.maxVelocity;
	}

    /// Hace desaparecer el vehiculo actual, y lo desactiva.
    public void CambioDeVehiculo()
    {
        cambioDeVehiculo = true;
        clonDeVehiculo[vehiculoActual].transform.position = new Vector3(-900, -900, -900);
        clonDeVehiculo[vehiculoActual].SetActive(false);

        vehiculoActual++;
        if (vehiculoActual >= 3 && !EstadosDelJuego.Instancia.terminado)
        {
            vehiculoActual = 2;
            Debug.Log("Deja de intentar cambiar más autos");
        }

        while (!vehicleUsado[vehiculoActual].funcionando)
        {
            vehiculoActual++;
            if (vehiculoActual >= 3 && !EstadosDelJuego.Instancia.terminado)
            {
                vehiculoActual = 2;
                Debug.Log("Deja de intentar cambiar más autos");
                break;
            }
        }
        SetPropiedades();
    }

    public void ActivateCar()
    {
        clonDeVehiculo[vehiculoActual].transform.position = this.transform.position;
        clonDeVehiculo[vehiculoActual].SetActive(true);
    }

    public void NuevoVehiculo()
    {
        if (cambioDeVehiculo)
        {
            cambioDeVehiculo = false;
            EstadosDelJuego.Instancia.invulnerable = true;
            clonDeVehiculo[vehiculoActual].transform.position = this.transform.position;
            clonDeVehiculo[vehiculoActual].SetActive(true);
            clonDeVehiculo[vehiculoActual].GetComponentInChildren<TrailCollisionerTest>().ReservarTrailsPeroYa();
        }
    }
}
