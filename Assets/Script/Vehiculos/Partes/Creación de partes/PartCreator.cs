﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PartCreator: MonoBehaviour {

    private PartHolder partHolder;
    private BaseObject createdObject;


    private static PartCreator partCreator;

    public static PartCreator Instance()
    {
        if (!partCreator)
        {
            partCreator = FindObjectOfType(typeof(PartCreator)) as PartCreator;
            if (!partCreator)
                Debug.LogError("There needs to be at least one GameObject with a PartCreator script attached to it!");
        }

        return partCreator;
    }


    private void Start()
    {
        partHolder = FindObjectOfType<PartHolder>() as PartHolder;
    }

    private void CreatePart()
    {
        int i = Random.Range(0, partHolder.objetosAparecibles.Count);

        createdObject = partHolder.objetosAparecibles[i];
    }

    public BaseObject CreatedObject
    {
        get
        {
            CreatePart();
            return createdObject;
        }
    }

}


















//private static PartCreator instacia = null;

//public static PartCreator Instancia
//{
//    get
//    {
//        if (instacia == null)
//        {
//            instacia = new PartCreator();
//        }
//        return instacia;
//    }
//}


//private InventoryManager myInventory;

//private PartCreator()
//{
//    if (myInventory == null)
//    {
//        myInventory = GameObject.FindWithTag("Inventory").GetComponent<InventoryManager>();
//    }
//}

//public void CreateNewPart (BaseObject part)
//{
//    int id = 1;

//    while (DiccionaryPartes.SearchKey(id))
//    {
//        if (id > 1000)
//        {
//            Debug.Log("Límite de partes alcanzadas");
//            return;
//        }
//        id++;
//    }

//    var newPart = Object.Instantiate(part);
//    Debug.Log(newPart.GetType());
//    newPart.name = newPart.name + id.ToString();
//    newPart.desgastePropio = 0;
//    newPart.id = id;

//    #if UNITY_EDITOR
//        AssetDatabase.CreateAsset(newPart, "Assets/ObjectsCreatedDuringRuntime/" + newPart.objectName + ".asset");
//    #else
//        AssetDatabase.CreateAsset(newPart, Path.Combine(Application.persistentDataPath, newPart.objectName + ".asset"));
//    #endif

//    myInventory.AddNewPart(newPart);

//}