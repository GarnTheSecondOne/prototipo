﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirajeController : MonoBehaviour {

    /// <summary>
    /// Se encarga de manejar el pivote del vehiculo.
    /// Al cambiarlo, cambia la posición del vehiculo y el tamaño del collider para cuadrarlo con el tipo de pivote/vehiculo
    /// </summary>

    private Transform[] myBoys; //Los transform de los hijos
    private Collider2D myCollider; //mi collider
    private Transform[] boysTransform; //los transform de los hijos... otra vez
    private Vector2 colliderOriginal; //el vector del collider original
    private Vector2 positionOriginal; //la posicion original

    public enum TipoDePivote 
    {
        normal, delantero, trasero
    }

    [SerializeField] public static TipoDePivote pivote = TipoDePivote.normal;
    [SerializeField] float mover = 0.06f; //cuanto debe moverse el sprite para cuadrarlo con el pivote


    public void SetParameters(TipoDePivote centro) //Pone los parametros desde la selección de vehiculo
    {
        pivote = centro;
        Iniciar();
        SetPivot();
    }

    private void Iniciar() //Inicia las variables
    {
        myBoys = transform.GetComponentsInChildren<Transform>();
        myCollider = GetComponent<Collider2D>();
        colliderOriginal = myCollider.offset;
        boysTransform = new Transform[myBoys.Length];
        for (int i = 0; i < myBoys.Length; i++)
        {
            boysTransform[i] = myBoys[i];
        }
        positionOriginal = transform.position;


        //SetPivot();
    }

    void SetPivot() //Pone los pivotes y cambia las posiciones para que queden en la posición inicial pero con el pivote cambiado 
    {
        switch (pivote)
        {
            case TipoDePivote.normal:
                for (int i = 0; i < myBoys.Length; i++)
                {
                    myBoys[i] = boysTransform[i];
                }
                myCollider.offset = colliderOriginal;
                transform.position = positionOriginal;
                break;

            case TipoDePivote.delantero:
                foreach (Transform lugar in myBoys)
                {
                    lugar.localPosition = new Vector3 (lugar.localPosition.x - mover, lugar.localPosition.y, lugar.localPosition.z);
                }
                myCollider.offset = new Vector2 (colliderOriginal.x - mover, colliderOriginal.y);
                transform.position = new Vector2(positionOriginal.x - mover * transform.localScale.x, positionOriginal.y);
                break;

            case TipoDePivote.trasero:
                foreach (Transform lugar in myBoys)
                {
                    lugar.localPosition = new Vector3(lugar.localPosition.x + mover, lugar.localPosition.y, lugar.localPosition.z);
                }
                myCollider.offset = new Vector2(colliderOriginal.x + mover, colliderOriginal.y);
                transform.position = new Vector2(positionOriginal.x - mover*transform.localScale.x, positionOriginal.y);
                break;

            default:
                for (int i = 0; i < myBoys.Length; i++)
                {
                    myBoys[i] = boysTransform[i];
                }
                myCollider.offset = colliderOriginal;
                transform.position = positionOriginal;
                break;
        }
    }

}
