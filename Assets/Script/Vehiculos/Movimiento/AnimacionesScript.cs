﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls; //Para controles de movil

public class AnimacionesScript : MonoBehaviour {

    /// <summary>
    /// Código que se encarga de animar el sprite al voltear.
    /// Tambien es quien se encarga de girar como tal todo.
    /// </summary>

    private float h, v; //horizontal y vertical
    private float angulo; //angulo del sprite

    private bool sinControl; //para quitar o poner el control del vehiculo

    Color[] originalColor;
    SpriteRenderer[] spriteRenderers;

	void Start ()
    {
        sinControl = false;
        ChoqueDealer.enFinal += PerderControl;
        ReinicioController.enReinicio += Reinicio;
        spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        originalColor = new Color[spriteRenderers.Length];
        for (int i = 0; i < originalColor.Length; i++)
        {
            originalColor[i] = spriteRenderers[i].color;
        }
    }

    private void OnDestroy()
    {
        ChoqueDealer.enFinal -= PerderControl;
        ReinicioController.enReinicio -= Reinicio;
    }

    void Update ()
    {
        if(sinControl || EstadosDelJuego.Instancia.Iniciando)
        {
            return;
        }
        if (EstadosDelJuego.Instancia.invulnerable)
        {
            for (int i = 0; i < originalColor.Length; i++)
            {
                Color Alpha = spriteRenderers[i].color;
                Alpha = new Color(Alpha.r, Alpha.g, Alpha.b, Alpha.a = ((Mathf.Sin(Time.time * 5) + 1) / 2));
                //Mathf.PingPong(Alpha.a, 1);
                spriteRenderers[i].color = Alpha;
            }
        }
        else
        {
            for (int i = 0; i < originalColor.Length; i++)
            {
                if (spriteRenderers[i].color != originalColor[i])
                {
                     spriteRenderers[i].color = originalColor[i];
                }
            }
        }

        h = CnInputManager.GetAxis("Horizontal"); //se detectar el axis del movimiento
        v = CnInputManager.GetAxis("Vertical");

        if (h != 0 || v != 0) //si los axis son diferentes de 0
        {
            ChangeAngle();
        }
    }

    private void ChangeAngle() //Cambia el angulo del sprite
    {
        angulo = Mathf.Atan2(v, h) * Mathf.Rad2Deg; //es el angulo de la tangente de v y h

        if (QuiereIrAtras(new Vector3(h, v).normalized))
        {
            return;
        }

        //transform.rotation = Quaternion.AngleAxis(angulo, Vector3.forward);
        if (VirajeController.pivote != VirajeController.TipoDePivote.normal) //Si el pivote del vehiculo no esta en el centro
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angulo, Vector3.forward), Time.deltaTime
                * (GetComponent<MovementPlayer>().Velocidad / GetComponent<MovementPlayer>().aceleracion)); //la rotación hace lerp entre la rotación actual y el nuevo angulo, teniendo en cuenta su velocidad y aceleración
        }
        else //si no lol
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angulo, Vector3.forward), Time.deltaTime
                * (GetComponent<MovementPlayer>().aceleracion)); //la misma rotación, pero solo tiene en cuenta la aceleración
        }

       // Debug.Log(GetComponent<MovementPlayer>().Velocidad/ GetComponent<MovementPlayer>().aceleracion);
    }

    private bool QuiereIrAtras(Vector3 movement)
    {
        float producto = Vector3.Dot(movement, transform.right);
        if (producto < -0.5f)
        {
            return true;
        }

        return false;
    }

    private void PerderControl() //Pierde el control de movimiento
    {
        sinControl = true;
    }



    private void Reinicio() //Reinicia todo
    {
        transform.rotation = Quaternion.identity;
        sinControl = false;
    }
}

