﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls; //Controles de movil
using System;

[RequireComponent(typeof(Rigidbody2D))]
public class MovementPlayer : MonoBehaviour {

    /// <summary>
    /// Todo lo relacionado a mover el vehiculo
    /// </summary>

    private Vector2 movement; //vector de movimiento
    private float minVelocity; //Velocidad minima
    private float h, v; //horizontal y vertical
    private Rigidbody2D rb; //el rigidbody2d de este objeto
    private bool evitarMovimient; //usado para que no retroceda


    private Vector2 anguloActual; 
    private Vector2 anguloAnterior;
    private float reductorDeVelocidad; //a cuanto se reduce la velocidad 

    [SerializeField]
    private float maxVelocity = 2; //Velocidad maxima a la que llegará
    [SerializeField]
    public float aceleracion; //la aceleración con la que se moverá

    private float desaceleracionPorDisparo = 10; //al disparar, cuanto desacelera
    private float desaceleracionPorGiro = 3; //al girar, en cuanto deberá desacelerarse 
    public static float knockbackStabilizer = 5f; //cuando le disparen, cuanto knockback va a atajar

    private bool tenerControl; //se tiene control del vehiculo?

    private Vector2 positionOriginal; //la posición original del vehiculo

    void Start()
    {
        try
        {
            rb = GetComponent<Rigidbody2D>(); //Detecta el Rigidbody2D del objeto
        }
        catch
        {
            print("No rigidbody2d");
        }

        positionOriginal = transform.position;
        tenerControl = true;
        StartRigidbody(); //Para organizar aspectos importantes del rigidbody
        anguloAnterior = new Vector2(h,v); //el angulo anterior desde el cual inicia a revisar si el angulo cambia es h y v
        reductorDeVelocidad = 1;
        minVelocity = 5;

        InvokeRepeating("DetectarAngulo", 0.3f, 0.3f); //cada 0.3 segundos revisa el angulo de giro

        ChoqueDealer.enFinal += PerderControl;
        ReinicioController.enReinicio += Reinicio;
        DisparadorScript.EnDisparo += ReducirVelocidad;
    }

    private void OnDestroy()
    {
        ChoqueDealer.enFinal -= PerderControl;
        ReinicioController.enReinicio -= Reinicio;
        DisparadorScript.EnDisparo -= ReducirVelocidad;
    }


    public void SetParameters(float aceleration, float MaxVelocidad, float velocidadAReducirDisparo, float velocidadAReducirGiro, float reductorKnock) // Se ponen los parametros desde el selector de vehiculo
    {
        aceleracion = aceleration;
        maxVelocity = MaxVelocidad;
        desaceleracionPorDisparo = velocidadAReducirDisparo;
        desaceleracionPorGiro = velocidadAReducirGiro;
        knockbackStabilizer = reductorKnock;
    }

    private void StartRigidbody() //Para organizar aspectos importantes del rigidbody
    {
        if (rb)
        {
            rb.gravityScale = 0; //la gravedad se pone en 0
        }
    }

	void FixedUpdate ()
    {
        if (!tenerControl || EstadosDelJuego.Instancia.Iniciando)
        {
            rb.velocity = Vector2.zero;
            return;
        }


        h = CnInputManager.GetAxis("Horizontal"); //Se dectan los axis 
        v = CnInputManager.GetAxis("Vertical");

        if (h != 0 || v != 0)
        {
            if (!TutorialController.tutorialHasBeenDone)
            {
                TutorialController.tutorialHasBeenDone = true;
            }

            if (evitarMovimient)
            {
                StopMoving(); //para que no siga moviendose 
            }
            else
            {
                StartMoving(); //Para hacer que se mueva
            }
        }
        else
        {
            StopMoving(); //Para detenerlo
        }


    }

    private void DetectarAngulo() //Detecta el angulo desde la ultima vez que se llamó este metodo
    {
        if ((h != 0 || v != 0) && tenerControl) //si se tiene control y no está quieto
        {
            anguloActual = new Vector2(h, v); //el angulo actual es h y v
            if (anguloAnterior == Vector2.zero) //Si es angulo anterior es 0,0
            {
                anguloAnterior = anguloActual; //se convierte en el angulo actual para evitar errores
            }
            float dot = Vector2.Dot(anguloAnterior, anguloActual); //se hace el producto punto entre el angulo anterior y el actual
            //Debug.LogFormat("Actual: {0}", dot);
            if (dot < 0.9) //Si dot es menor a 0.9
            {
                //Debug.Log(Vector2.Angle(anguloActual, anguloAnterior));
                reductorDeVelocidad += (Vector2.Angle(anguloActual, anguloAnterior)/360) * desaceleracionPorGiro; //El reductor de velocidad se suma a ser la desaceleración por giro por el angulo entre los dos vectores especificados
                if (dot < -0.5f && VehiculoActual.type != Tipo.moto) //Si el angulo pasa más de los 120 grados
                {
                    StopMoving();
                    evitarMovimient = true;
                    StartCoroutine(VamosAEsperar()); //Se cambia esto para que se pueda avanzar un poco antes de girar
                }
            }
            anguloAnterior = new Vector2(h, v); //El angulo anterior se vuelve el angulo actual
        }
    }

    private IEnumerator VamosAEsperar()
    {
        yield return new WaitForSeconds(1);
        evitarMovimient = false;
    }

    private void StartMoving() //Para hacer que se mueva
    {
        if (evitarMovimient)
        {
            h = 0;
            v = 0;
        }

        movement = new Vector2(h, v); //el vector de movimiento es igual a h y v
        movement = movement.normalized; //se normaliza este vector

        if (QuiereIrAtras(movement) == true)
        {
            return;
        }

        if (reductorDeVelocidad < 1)
        {
            reductorDeVelocidad = 1; //Si el reductor es menor a 1, se vuelve 1
        }
        rb.velocity = Vector2.Lerp(rb.velocity, (movement * maxVelocity * 10)/reductorDeVelocidad, (Time.deltaTime * 2)); //la velocidad hace lerp entre la actual y el movimiento por la velocidad por 10 dividida el reductor, en un tiempo multiplicado por la aceleración
        if (reductorDeVelocidad > 1)
        {
            QuitarReductor(); //Si el reductor no es 1, se empieza a restar
        }
    }

    private bool QuiereIrAtras(Vector3 movement)
    {
        float producto = Vector3.Dot(movement, transform.right);
        if (producto < 0)
        {
            return true;
        }

        return false;
    }

    public float Velocidad //Getter de la velocidad del rigidbody
    {
        get
        {
            return rb.velocity.magnitude;
        }
    }

    private void QuitarReductor() //Reduce el reductor de velocidad en el tiempo por la aceleración 
    {
        reductorDeVelocidad -= Time.deltaTime * aceleracion;
    }

    private void ReducirVelocidad() //Al disparar, reduce la velocidad momentaneamente 
    {
        rb.velocity = rb.velocity/desaceleracionPorDisparo;
    }

    private void StopMoving() //Para reducir el movimiento al dejar de acelerar
    {
        rb.velocity = Vector2.Lerp(rb.velocity, (transform.right * minVelocity), (Time.deltaTime * aceleracion)); //Intentando suavizar un poco el freno
    }

    private void PerderControl() //Para que no se pueda controlar más el vehiculo
    {
        StopMoving();
        tenerControl = false;
    }

    private void Reinicio() //Reinicia el vehiculo
    {
        tenerControl = true;
        anguloAnterior = Vector2.zero;
        transform.position = positionOriginal;
    }

    private void Knockback(Vector2 direccion) //Crea knockback al ser golpeado
    {
        rb.AddForce(direccion/knockbackStabilizer, ForceMode2D.Impulse);
    }
}
