﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BalaSalirDisparada : MonoBehaviour {

    /// <summary>
    /// Este codigo se agrega a cada bala. 
    /// Hace que salga disparada y dezaparezca
    /// </summary>

    [HideInInspector]
    public bool iniciado; //está iniciada la bala?
    [HideInInspector]
    public int index; //el indice de esta bala
    [SerializeField]
    public float duracionDisparo = 2f; //cuanto dura 
    [SerializeField]
    private float velocidadBala = 10f; //La velocidad con la que avanza la bala

    private bool noMas; //Para que el disparo pueda desaparecer después de un tiempo
    private float tiempoPasado; //el tiempo que ha pasado desde que se disparó la vala
    Vector2 direccion; //dirección a la cual se dirije la bala
    Rigidbody2D rb2d; //el rigidbody2d de este objeto
    

    public delegate void EventosDeDisparo(int yo);
    public static event EventosDeDisparo EnDejarDeExistir;
    public static event EventosDeDisparo EnOcultar;
    //Eventos para que las balas hagan cosas, enviados a la pool de balas

	void Awake ()
    {
        rb2d = GetComponent<Rigidbody2D>(); 
        Prepare();
    }

    private void Start()
    {
        BalasTrigger.EnGolpear += OcultarDisparo;
    }

    private void Prepare()
    {
        tiempoPasado = 0; 
        iniciado = false; //no esta iniciado
        noMas = false; //no se ha disparado
        rb2d.gravityScale = 0; //la gravedad del objeto es 0
    }

	void Update ()
    {
		if (iniciado && !noMas) //si se ha iniciado pero no ha disparado
        {
            Disparado(); //solo se llama una vez cuando se dispara
            noMas = true;
        }
        if (noMas)
        {
            tiempoPasado += Time.deltaTime;
        }
        if (tiempoPasado >= duracionDisparo)
        {
            QuitarDisparo(); //se desaparece el disparo después de un tiempo
        }
	}

    private void Disparado()
    {
        direccion = transform.right; //la dirección es hacia el "frente" (en este caso, la derecha)
        rb2d.velocity = direccion * (1000 * velocidadBala) *  Time.deltaTime; //la velocidad es la dirección por 1000 por la velocidad por el tiempo
    }

    private void OcultarDisparo(int indice) //Para ocultar este disparo
    {
        EnOcultar(indice); //para que oculte el disparo, más no quite por completo la bala del lugar (para evitar problemas al darle a los objetivos)
    }

    private void QuitarDisparo()
    {
        tiempoPasado = 0;
        rb2d.velocity = Vector2.zero;
        iniciado = false;
        noMas = false;
        EnDejarDeExistir(index); //Se envia a la pool para que quite este disparo
    }
}
