﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgregarmeAGuns : MonoBehaviour {

    /// <summary>
    /// Se le pone a los disparadores del vehiculo. 
    /// Se encarga de agregar este punto desde el cual se dispara a "lugares desde los que disparar" de Guns (el script disparador)
    /// </summary>

    static DisparadorScript disparador; //solo hay 1 disparador

	private void Awake ()
    {
        disparador = GetComponentInParent<DisparadorScript>();  //se busca el disparador script en los padres
        Agregarme();

        DisparadorScript.EnNoHayLugaresParaDisparar += Agregarme;
	}

    private void OnDestroy()
    {
        DisparadorScript.EnNoHayLugaresParaDisparar -= Agregarme;
    }


    private void Agregarme() //Se agrega a la lista de lugares por los que disparar del disparador script
    {
        disparador = GetComponentInParent<DisparadorScript>();
        //disparador.lugaresPorLosQueDisparar.Add(this.gameObject);
    }
}
