﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalasPoolManager : MonoBehaviour {

    /// <summary>
    /// Pool de las balas
    /// Controla su existencia
    /// </summary>

    public GameObject[] balas; //Todas las balas

    private int index; //el indice de la bala al disparar

	void Start ()
    {
        if (balas == null)
        {
            Debug.Log("Por favor agregar balas al pool");
        }

        BalaSalirDisparada.EnDejarDeExistir += DevolverUnaBala;
        BalaSalirDisparada.EnOcultar += DesaparecerUnaBala;
        DisparadorScript.EnDisparado += SacarUnaBala;
        

        IniciarBalas();
	}

    private void OnDestroy()
    {
        BalaSalirDisparada.EnDejarDeExistir -= DevolverUnaBala;
        BalaSalirDisparada.EnOcultar -= DesaparecerUnaBala;
        DisparadorScript.EnDisparado -= SacarUnaBala;
    }

    private void IniciarBalas()
    {
        for (int i = 0; i < balas.Length; i++)
        {
            balas[i].gameObject.transform.position = new Vector2(-900, -900); //Se inician las balas en la posición por allá
            balas[i].gameObject.SetActive(false); //Se desactivan
            balas[i].GetComponent<BalaSalirDisparada>().index = i; //se le pone el indice correspondiente a i
            balas[i].GetComponent<BalasTrigger>().indexPropio = i; //lo mismo, pero en el trigger
        }
    }

    private void SacarUnaBala(Transform lugar, float distancia, float knockback, float magnitud, DisparadorScript.NaturalezaArma naturaleza)
    {
        if (index < balas.Length) //si el indice es menor a la longitud del array de balas
        {
            balas[index].gameObject.SetActive(true); //se activa la bala
            balas[index].gameObject.transform.SetPositionAndRotation(lugar.position, lugar.rotation); //se le asigna un lugar y una rotación
            balas[index].GetComponent<BalasTrigger>().knockback = knockback; //se le pone su knockback
            balas[index].GetComponent<BalasTrigger>().radio = magnitud; //se le pone su magnitud/radio de área que afecta
            balas[index].GetComponent<BalasTrigger>().naturalezaDisparo = naturaleza; //se pone la naturaleza del disparo
            balas[index].GetComponent<BalaSalirDisparada>().iniciado = true; //se inicia el disparo
            balas[index].GetComponent<BalaSalirDisparada>().duracionDisparo = distancia; //se le pone una distancia
            index++; //se aumenta el indice
        }
        else //si no
        {
            index = 0; //el indice vuelve a 0
            SacarUnaBala(lugar, distancia, knockback, magnitud, naturaleza); //se vuelve a llamar este codigo
        }
    }

    private void DevolverUnaBala(int cualDeTodas) //Para devolver una bala a la pool
    {
        balas[cualDeTodas].gameObject.transform.position = new Vector2(-900, -900); //se envia al mundo de nadie
        balas[cualDeTodas].GetComponent<Renderer>().enabled = true;  //se activa su renderer
        balas[cualDeTodas].GetComponent<Collider2D>().enabled = true; //se activa su collider
        balas[cualDeTodas].gameObject.SetActive(false); //se desactiva
    }

    private void DesaparecerUnaBala(int cual) //Para desaparecer visualmente una bala
    {
        balas[cual].GetComponent<Rigidbody2D>().velocity = Vector2.zero; //su velocidad se pone en 0
        balas[cual].GetComponent<Renderer>().enabled = false; //se desactiva su renderer
        balas[cual].GetComponent<Collider2D>().enabled = false; //se desactiva su collider
    }
}
