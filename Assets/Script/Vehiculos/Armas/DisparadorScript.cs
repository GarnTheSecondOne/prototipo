﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls; //Controles de movil
using UnityEngine.UI;

public class DisparadorScript : MonoBehaviour {

    /// <summary>
    /// Este codigo es quien se encarga como tal de disparar las balas y poner los parámetros a cada una
    /// </summary>

    public enum NaturalezaArma //Tipo de naturaleza de arma
    {
        hole,
        tail
    }


    public List<GameObject> lugaresPorLosQueDisparar = new List<GameObject>(3); //Todos los lugares por los que va a disparar el arma

    [SerializeField]
    private float tiempoEntreDisparos = 5; //tiempo entre... cada disparo....
    [SerializeField]
    private Text textoRecarga; //texto en el que se muestra que está recargandose


    private float tiempoDeDuracion = 0.2f; //duración del disparo
    private float knobackForce = 1f; //knockback del disparo
    private float magnitudDisparo = 3f; //radio de efecto del disparo
    private NaturalezaArma naturaleza = NaturalezaArma.hole; //naturaleza del disparo
    private int armasDisponibles = 2; //Numero de proyectiles

    private float tiempoActual; //tiempo actual
    private bool heDisparado; //se ha disparado una bala
    private bool funcionar; //debo funcionar

    public delegate void EventoDeDisparo(Transform donde, float cuanto, float knock, float magni, NaturalezaArma natu);
    public static event EventoDeDisparo EnDisparado;
    //Evento que sucede al disparar, se envia al pool de balas

    public delegate void EnCuantoDispara();
    public static event EnCuantoDispara EnDisparo;
    public static event EnCuantoDispara EnNoHayLugaresParaDisparar;
    //Evento que se envía en cuanto dispara,no me acuerdo quien lo recibe

	void Start ()
    {
        if (textoRecarga == null)
        {
            textoRecarga = GameObject.Find("DisparoTest").GetComponent<Text>();
        }

        if (lugaresPorLosQueDisparar.Count == 0)
        {
            AgregarmeAGuns[] guns = gameObject.GetComponentsInChildren<AgregarmeAGuns>();

            for (int i = 0; i < guns.Length; i++)
            {
                lugaresPorLosQueDisparar.Add(guns[i].gameObject);
            }
        }


        heDisparado = false;
        tiempoActual = 0;
        funcionar = true;
        textoRecarga.text = "";

        if (lugaresPorLosQueDisparar == null)
        {
            Debug.Log("Las armas no han sido agregadas al disparador");
        }
	}

    public void SetDisparos(float frecuencia, float duracion, float magnitud, NaturalezaArma naturalezaNatural, int numeroDeProyectiles) //Se settean las armas desde la selección de vehiculo
    {
        if (lugaresPorLosQueDisparar.Count == 0)
        {
            lugaresPorLosQueDisparar.Clear();
            AgregarmeAGuns[] guns = gameObject.GetComponentsInChildren<AgregarmeAGuns>();

            for (int i = 0; i < guns.Length; i++)
            {
                lugaresPorLosQueDisparar.Add(guns[i].gameObject);
            }
        }
        tiempoEntreDisparos = frecuencia;
        tiempoDeDuracion = duracion;
        magnitudDisparo = magnitud;
        naturaleza = naturalezaNatural;
        armasDisponibles = numeroDeProyectiles;
        SetWeapons();
        //knobackForce = knockbackThing;
    }

    private void SetWeapons()
    {
        switch (armasDisponibles)
        {
            case 1:
                lugaresPorLosQueDisparar[0].SetActive(false);
                lugaresPorLosQueDisparar[1].SetActive(false);
                lugaresPorLosQueDisparar[2].SetActive(true);
                break;

            case 2:
                lugaresPorLosQueDisparar[0].SetActive(true);
                lugaresPorLosQueDisparar[1].SetActive(true);
                lugaresPorLosQueDisparar[2].SetActive(false);
                break;

            case 3:
                lugaresPorLosQueDisparar[0].SetActive(true);
                lugaresPorLosQueDisparar[1].SetActive(true);
                lugaresPorLosQueDisparar[2].SetActive(true);
                break;

            default:
                lugaresPorLosQueDisparar[0].SetActive(true);
                lugaresPorLosQueDisparar[1].SetActive(true);
                lugaresPorLosQueDisparar[2].SetActive(true);
                break;
        }
    }
	

	void Update ()
    {
		if (!funcionar || EstadosDelJuego.Instancia.Iniciando)
        {
            return; //si no está funcionando, larguese
        }

        if (heDisparado)
        {
            tiempoActual -= Time.deltaTime;
            if (tiempoActual <= 0)
            {
                tiempoActual = 0; //shut up
                heDisparado = false;
            }
            SetText();
        }

        if (CnInputManager.GetButtonDown("Jump")) //Cuando se presiona "Jump", o espacio, o el botón de la pantalla
        {
            if (tiempoActual <= 0 && !heDisparado) //Si el tiempo actual es igual o menor a cero y no se ha disparado
            {

                Disparar(); //se dispara
                tiempoActual = tiempoEntreDisparos;
            }
        }

	}

    private void Disparar()
    {
        if (!TutorialController.shootingHasBeenDone && TutorialController.tutorialHasBeenDone)
        {
            TutorialController.shootingHasBeenDone = true;
        }

        for (int i = 0; i < lugaresPorLosQueDisparar.Count; i++)
        {
            if (lugaresPorLosQueDisparar[i].activeInHierarchy)
            {
                EnDisparado(lugaresPorLosQueDisparar[i].transform, tiempoDeDuracion, knobackForce, magnitudDisparo, naturaleza); //Por todos los lugares por los que disparar, se envía una bala desde la pool de balas
            }
        }
        heDisparado = true;

        EnDisparo();
    }

    private void DejarDeFuncionar()
    {
        funcionar = false;
    }

    private void SetText() //Se pone el texto de que está recargando
    {
        textoRecarga.text = string.Format("RECHARGING... {0}",(tiempoActual+1).ToString("F2")); //Pone el texto y de tiempo le dael tiempo actual (+1 porque se ve más bonito)
        if (tiempoActual <= 0)
        {
            textoRecarga.text = "";
        }
    }
}
