﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalasTrigger : MonoBehaviour {

    /// <summary>
    /// Necesario en cada bala, se encarga de los eventos que suceden al detectar una "colisión" (aunque es un trigger)
    /// </summary>


    Collider2D[] hitColliders; //los colliders que se van a detectar al golpear


    public delegate void MeHeGolpeado(int indice);
    public static event MeHeGolpeado EnGolpear;
    //Ayuda! Me he golpeado! Se envía al pool de balas

    public delegate void HeGolpeado(int indice, int dueño);
    public static event HeGolpeado EnDesaparecerLinea;
    public static event HeGolpeado EnDesaparecerAtrasLinea;
    //Para desaparecer los traces. Se envia al trailcollisioner

    public delegate void JugadoresGolpeados(Vector2 direction);
    public static event JugadoresGolpeados EnCrearKnoback;
    //Para darle knockback a los jugadores 

    int index; //el indice del trace al que se golpea
    int dueñoDeTrail; //el dueño del trace al que se golpea
    public int indexPropio; //el indice de esta bala
    public float knockback; //el knockback que se va a generar
    public float radio; //el radio en el que afecta el disparo
    public DisparadorScript.NaturalezaArma naturalezaDisparo; //la naturaleza del disparo

    //private Vector2 miDireccion;
    private Vector2 direccion; //la dirección a la que va esta bala


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Lineas")) //Si el layer de lo que se golpea es "Lineas"
        {
            EnGolpear(indexPropio); //Me he golpeado
            if (naturalezaDisparo == DisparadorScript.NaturalezaArma.hole)
            {
                EnviarEsfera(); //Si es un hueco, se usa overlapCircle (es como el spherecasting)
            }
            else
            {
                EliminarDesdeAca(collision); //Si es linea, se desaparecen todos los traces hasta el final
            }
        }
        else if (collision.CompareTag("Enemy")) //Si el tag es del enemigo
        {
            direccion = GetComponent<Rigidbody2D>().velocity; //la dirección es la velocidad del rigidbody
            collision.SendMessage("Knockback",direccion * knockback); //se le envía el mensaje a lo que se colisionó de que haga knockback a una dirección
            EnGolpear(indexPropio); 
        }
        else if (!collision.CompareTag("Player"))
        {
            EnGolpear(indexPropio); //Si golpeó otra cosa, que desaparezca y ya
        }
    }

    private void RevisarPosicion()
    {
        Vector2 direccion = GetComponent<Rigidbody>().velocity; //No me acuerdo esto por qué está aca
    }

    private void EnviarEsfera()
    {
        //Physics2D.OverlapCircleNonAlloc(transform.position, 5f, hitColliders, LayerMask.NameToLayer("Lineas"));
        LayerMask myLayermask = 1 << LayerMask.NameToLayer("Lineas"); //para que se pueda indicar a qué afecta el overlapCircle, se obtiene el numero de la layer

        hitColliders = Physics2D.OverlapCircleAll(transform.position, radio, myLayermask); //Se envia un overlap circle en la posicion de la bala, en un radio, afectando solo a los traces
        for (int i = 0; i < hitColliders.Length; i++) 
        {
            if (hitColliders[i] != null)
            {
                if (hitColliders[i].gameObject.GetComponent<TriggerTrail>())
                {
                    index = hitColliders[i].gameObject.GetComponent<TriggerTrail>().indice; //a cada uno de los golpeados se les ve el indice
                    dueñoDeTrail = hitColliders[i].gameObject.GetComponent<TriggerTrail>().dueño; //a cada uno de los golpeados se les ve el dueño
                    EnDesaparecerLinea(index, dueñoDeTrail); //se desaparece todos los golpeados 
                }
            }
        }

    }

    private void EliminarDesdeAca(Collider2D collider)
    {
        index = collider.gameObject.GetComponent<TriggerTrail>().indice; //al golpeado se le ve el indice
        dueñoDeTrail = collider.gameObject.GetComponent<TriggerTrail>().dueño; //al golpeado se le ve el dueño 
        EnDesaparecerAtrasLinea(index, dueñoDeTrail); //se elimina a todos los tracess desde acá
    }

}
