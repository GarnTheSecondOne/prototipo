﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoqueDealer : MonoBehaviour {

    /// <summary>
    /// Este código se encarga de manejar todo lo que son los choques y quien se choca
    /// </summary>


    private Rigidbody2D rb;
    private static int index;

    public delegate void Finalizar();
    public static event Finalizar enFinal;
    public static event Finalizar enGanar;
    public static event Finalizar enPerder;
    //Se envía a varios códigos, para que hagan cosas extra que enReiniciar no funcionan

    public delegate void Reiniciador(int evitarQueSuccedaMucho);
    public static event Reiniciador enReiniciar;
    //Se envía a ReinicioController y a otros

    public Text texto;

    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        index = 0;

        //Eventos a los que está suscrito el ChoqueDealer
        TriggerTrail.EnChocado += SeHaChocado;
        ReinicioController.enReinicio += Reinicio;
        TriggerTrail.EnChoque += HaGanado;

        TriggerBordes.EnBotChoque += HaGanado;
        TriggerBordes.EnTuChoque += SeHaChocado;

        ScoreController.EnFinalDelJuego += Terminar;

        if (texto == null)
        {
            texto = GameObject.Find("TestText").GetComponent<Text>(); //Busca el texto para poder poner si se gana o pierde
        }
        texto.text = "";
	}


    private void OnDestroy()
    {
        TriggerTrail.EnChocado -= SeHaChocado;
        ReinicioController.enReinicio -= Reinicio;
        TriggerTrail.EnChoque -= HaGanado;

        TriggerBordes.EnBotChoque -= HaGanado;
        TriggerBordes.EnTuChoque -= SeHaChocado;

        ScoreController.EnFinalDelJuego -= Terminar;
    }

    //El jugador se ha chocado
    private void SeHaChocado (int cuantasVeces)
    {
        index += cuantasVeces; //Para evitar que se llame el codigo varias veces
        if (index > 1)
        {
            return;
        }
        Detener();

        texto.text = "VEHICLE LOST";

        enPerder();
        enFinal();
        enReiniciar(1);
	}

    //Reinicia todo lo de ChoqueDealer
    private void Reinicio()
    {
        index = 0;
        texto.text = "";
    }

    //El bot se ha chocado
    private void HaGanado (int cuantasVeces)
    {
        index += cuantasVeces;
        if (index > 1)
        {
            return;
        }
        Detener();

        texto.text = "ENTR0-PY DESTROYED";
        enGanar();
        enFinal();
        enReiniciar(1);
    }

    private void Detener()
    {
        rb.velocity = Vector3.zero; //Se le pone la velocidad en zero
        if (rb.velocity != Vector2.zero)
        {
            rb.velocity = Vector3.zero;
        }
    }

    private void Terminar(bool heGanado)
    {
        if (heGanado)
        {
            texto.text = "YOU WIN";
        }
        else
        {
            texto.text = "YOU LOST";
        }
    }

}
